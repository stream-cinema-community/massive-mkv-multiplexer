#########################################
#### Webshare.cz Togen Getter ###########
#### Created By Michalss 2022 ###########
#########################################

$xdoc = [xml] (Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
$username = $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER
$password = $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD

$SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD | ConvertTo-SecureString
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword))))


$ExoPowershellModule = "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\md5_crypt_ps.dll"
$ModulePath = [System.IO.Path]::Combine($PSExoPowershellModuleRoot, $ExoPowershellModule);
Get-ChildItem -path  "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\*.*" -Recurse | Unblock-File
Import-Module $ModulePath;

function GetHttpContent($body,$uri) {
    $contentType = 'application/x-www-form-urlencoded' 
    [xml]$cont = Invoke-WebRequest -Method POST -Uri $uri -body $body -ContentType $contentType
    return $cont
}

function Md5Hasher($someString){

    $md5 = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    $utf8 = New-Object -TypeName System.Text.UTF8Encoding
    $hash = [System.BitConverter]::ToString($md5.ComputeHash($utf8.GetBytes($someString)))
    return $hash

}

## 1. First need to request for salt (POST /api/salt/) username_or_email={The username or email address of the user.}
$bodySalt = @{username_or_email=$username;
          charset='UTF-8'}
$salted = GetHttpContent $bodySalt "https://webshare.cz/api/salt/"
$salt = $salted.response.salt

## 2. get MD5Cypt
$md5crypt = Get-MD5Crypt -password $password -salt $salt

## 3. Get SHA1
$mystream = [IO.MemoryStream]::new([byte[]][char[]]$md5crypt.md5_crypt)
$sha1temp = Get-FileHash -InputStream $mystream -Algorithm SHA1
$sha1 = $sha1temp.Hash.ToLower()

## 3. Get Digest and hash it
$digest = (Md5Hasher "$($username):Webshare:$($sha1)").replace("-","").ToLower()

## 4. get token
$bodyToken = @{username_or_email=$username;
          password=$sha1;
          digest=$digest;
          charset='UTF-8';
          keep_logged_in=1}
[xml]$tokenize = GetHttpContent $bodyToken "https://webshare.cz/api/login/"
$token= $tokenize.response.token

## Console Output
write-host "###################################"
write-host "## Salt: $salt"
write-host "## Md5_crypt: $md5crypt.md5_crypt"
write-host "## SHA1: $sha1"
write-host "## DIGEST hash: $($digest)"
write-host "###################################"
write-host "## Your Token is: $($token)"
