
. .\WSToken.ps1
$CURLEXE = "curl.exe"
$wstoken = $token

function ws_upload_theFile 
{
    param 
    (
        [Parameter(Mandatory=$True, ValueFromPipeline = $true)]
        [string] $MovieName,
        [string] $folder = "/",
        [string] $username_or_email,
        [Parameter(Mandatory=$True, ValueFromPipeline = $true)]
        [string] $FilePath,
        [string] $private = 1,
        [string] $adult = 0
    )

    $ws='https://webshare.cz/api/upload_url?wst='+$wstoken
    $WebshareURL = Invoke-RestMethod -Uri $ws -Method GET 
    $uri = $WebshareURL.response.url
    $CurlArguments = '--request', 'POST', 
    "$uri",
        '--header', "'content-type: multipart/form-data'",
        '--form', "name=$MovieName",
        '--form', "wst=$wstoken",
        '--form', "folder=$folder",
        '--form', "private=$private",
        '--form', "private=$adult",
        '--form', "file=@$FilePath",
        ,'-v'


    #$web = curl --progress-bar @CurlArguments
    $web = & $CURLEXE --progress-bar @CurlArguments
    $webbetter = $web | ConvertFrom-Json
    return $webbetter.ident
}

 
#Write-Output """season"",""episode"",""wsid""" >> $FolderSerial\wsid.csv
#$FolderNumber = Read-Host -Prompt 'Zadejte ��slo s�rie'

foreach ($item in $files) 
{
    
    #$FolderCount = Get-ChildItem -Directory $FolderSerial | Measure-Object 
    
    $measure = Get-Content $FolderSerial\wsid.csv | Measure-Object 
    $lines = $measure.Count
    $wsUUID=ws_upload_theFile -MovieName $item.Name -FilePath $item.FullName
    Write-Output   """${FolderNumber}"",""${lines}"",""$wsUUID""" >> $FolderSerial\wsid.csv
    
    
}



