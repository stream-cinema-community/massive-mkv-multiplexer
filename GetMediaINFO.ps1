Copy-Item  .\MediaInfo.xml -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\MediaInfo.xml"
$Video = Get-ChildItem $FilesMedia.FullName
[int]$subAudio=Get-MediaInfoValue $FilesMedia.FullName -Kind General -Parameter 'AudioCount'
[int]$subCount=Get-MediaInfoValue $FilesMedia.FullName -Kind General -Parameter 'TextCount'
$i=0
$FinalCount=$subCount
$file = "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\MediaInfo.xml"

$xmlfile = [XML](Get-Content $file)

$ID=$subAudio+1
for ($i = 0; $i -lt $FinalCount; $i++ )
{

    $Lang = Get-MediaInfoValue $Video.Fullname -Kind Text -Index $i -Parameter 'Language/String'
    
    if($lang -eq ""){
        $lang="UND"
    }
    $newbooknode = $xmlfile.catalog.AppendChild($xmlfile.CreateElement("MediaInfo"))
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("ID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($ID)) | Out-Null
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Lang"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($Lang)) | Out-Null
    $xmlfile.save($file)
    $ID++
}

$xmlfile = [XML](Get-Content $file)
$LangID = @()
$LangID=$xmlfile.catalog.MediaInfo |Select-Object Id,Lang 

$Filter = $LangID | Where-Object -Property Lang -In 'Czech','Slovak','English'
$Filter."ID"

$Test = @($Filter.'ID')

$ID = $test -join ","
$SubOnly = "--subtitle-tracks $ID"
if($ID -eq ""){
    $SubOnly=""
}

