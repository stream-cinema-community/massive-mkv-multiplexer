Add-Type -AssemblyName PresentationFramework, PresentationCore, WindowsBase, System.Windows.Forms, System.Drawing
#OK
Function MakeNewForm {
	$Form.Close()
	#$Form.Dispose()
	MakeForm
}

Function MakeForm {
$xamlfile = "MainWindow.xaml" 
$inputXAML=Get-Content -Path $xamlfile -Raw
$inputXAML=$inputXAML -replace 'mc:Ignorable="d"','' -replace "x:N","N" -replace '^<Win.*','<Window'
[XML]$XAML=$inputXAML
$inputXAML.MainWindow

#Read XAML 
$XAMLReader= New-Object System.Xml.XmlNodeReader $XAML
try{
    $Form=[Windows.Markup.XamlReader]::Load($XAMLReader)
} catch {
    Throw "Unable to load Windows.Markup.XamlReader. Double-check syntax and ensure .net is installed."
}
 
#===========================================================================
# Load XAML Objects In PowerShell
#===========================================================================
 
$XAML.SelectNodes("//*[@Name]") |
ForEach-Object {
    Set-Variable -Name "WPF$($_.Name)" -Value $Form.FindName($_.Name)
}


$Gitlab = Invoke-RestMethod -Uri "https://gitlab.com/api/v4/projects/34251754/releases"
$Release= $Gitlab[0].description
$URI = "https://gitlab.com/stream-cinema-community/massive-mkv-multiplexer/-/raw/main/EXE/SCCMKVTooll("+$Gitlab[0].name+").EXE?inline=false"

if($WPftxtVersion.Content -ne $Gitlab[0].name){$WPFbtnRelease.Content="!! Je k dispozici nova verze !!"; $WPFbtnRelease.Visibility="Visible"
$WPFbtnRelease.add_Click({
    $Outfile = "$($env:USERPROFILE)\Downloads\SCCMKVTooll("+$Gitlab[0].name+").EXE"
    Invoke-RestMethod -Uri $URI -OutFile $Outfile
    [System.Windows.MessageBox]::Show("Nova verze byl stazena do adresare Download",'New version','Ok','Information')
    $WPFconsole.IsSelected = $true
    $mkvmergeoutput = "$Release"
    $WPFrichbox.AppendText($mkvmergeoutput)  #| Out-String -Width 3))

})
}

# Kontrola verze MKVToolnix

$Split=($wpftxtInputFilePath.text).split('\')
$FolderMKV=$split[0]+"\"+$split[1]+"\"+$split[2]+"\doc\News.txt"
$Version= Get-Content $FolderMKV | Select-Object -First 1
$WPFconsole.IsSelected = $true
$WebResponse = Invoke-WebRequest "https://mkvtoolnix.download/doc/NEWS.md"
if($WebResponse.Content -notlike "*$Version*"){
    $WPFrichbox.AppendText(("Byla detekovana starsi verze MKVToolnix nez je aktualni na webu. Vase verze je $Version. Pro lepsi kompatibilitu si stahnete novou verzi programu na webu https://mkvtoolnix.download/downloads.html " | Out-String ))
    $WPFrichbox.ScrollToEnd()
}
#Get-Variable WPF*
Start-Transcript  -Path "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\PS.log" -ErrorAction SilentlyContinue
$ExistConf = Get-ChildItem -Path "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" -Name Settings.xml
if(-not $ExistConf -eq ".\Settings.xml"){
    Copy-Item  .\Settings.xml -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" 
}
$xdoc = [xml] (Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
$ExistConf = Get-ChildItem -Path "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" -Name MainWindow.xaml
Copy-Item  .\MainWindow.xaml -Force "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" 
Copy-Item  .\Konfigurace.xaml -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" 
Copy-Item  .\WS_powershell.ps1 -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" 
Copy-Item  .\WSToken.ps1 -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" 
Copy-Item  .\MediaInfo.xml -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\MediaInfo.xml"
if($xdoc.SCCMKVTOOL.VERSION -ne "2.4"){
Remove-Item "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\settings.xml"
Copy-Item  .\Settings.xml -Force -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL\Settings.xml" 
}


# Instalace PSGallery a Get-MediaInfo
[Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12
#Register-PSRepository -Default -Verbose


Try {
    Import-Module PowerShellGet -Force -Erroraction stop
    Write-Host "Modul PSGallery je nainstalovn"
        
    
}
Catch {
    Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
    Write-Host "Bude instalovn modul PowershellGet"
    Install-Module -Name PowershellGet
        
   
}

Try {
    Import-Module Get-MediaInfo -Force -Erroraction stop
    Write-Host "Modul Get-MediaInfo je nainstalovn"
            
    }
Catch {
    Write-Host "Bude nainstalovn modul Get-MediaInfo"
    Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
    Install-Module -Name Get-MediaInfo 
        
   
    }

$ExistConf = Get-ChildItem -Path "$($env:USERPROFILE)\AppData\Local\" -Name SCCMKVTOOL
if ($ExistConf.Count -eq "0") {
New-Item -Path "$($env:USERPROFILE)\AppData\Local\" -Name "SCCMKVTool" -ItemType Directory
Copy-Item  "Settings.xml" -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL"
}
$ExistConf = Get-ChildItem -Path "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL" -Name md5_crypt_ps.dll
if ($ExistConf.Count -eq "0") {
Copy-Item  "md5_crypt_ps.dll" -Destination "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL"
}

$xdoc = [xml] (Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
$WPFtxtSetFolder.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH
$WPFtxtInputFilePath.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.MKVMERGE
$WPFchkcHash.IsChecked = $True
$WPFchkcOrigin.IsChecked = $True
$WPFchkcName.IsChecked = $True
$WPFchkcTags.IsChecked = $True
$WPFchkcUpload.IsChecked = $True
$WPFchkcSubOnly.IsChecked = $True
$WPFchkcSCC.IsChecked = $True

if($xdoc.SCCMKVTOOL.CONFIGURATIONS.HASH -eq "False"){
    $WPFchkcHash.IsChecked = $False}
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.ORIG -eq "False"){
    $WPFchkcOrigin.IsChecked = $False}
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.CHANGENAME -eq "False"){
    $WPFchkcName.IsChecked = $False}   
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.TAGS -eq "False"){
    $WPFchkcTags.IsChecked = $False}   
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.UPLOADWS -eq "False"){
    $WPFchkcUpload.IsChecked = $False}
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.SUBONLY -eq "False"){
    $WPFchkcSubOnly.IsChecked = $False}
if($xdoc.SCCMKVTOOL.CONFIGURATIONS.SCC -eq "False"){
    $WPFchkcSCC.IsChecked = $False}
$File = Get-ChildItem $WPFtxtSetFolder.Text -Recurse -File | where-object Length -gt 5200kb
$Subs = Get-ChildItem $WPFtxtSetFolder.Text -Exclude $File
$WPFtxtNumberFile.Text = $File.Count
$WPFtxtNumberSubs.Text = $Subs.Count   


#Funkce Get-Randomword n?hodn? heslo
function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length,
        [int] $amountOfNonAlphanumeric = 1
    )
    Add-Type -AssemblyName 'System.Web'
    return [System.Web.Security.Membership]::GeneratePassword($length, $amountOfNonAlphanumeric)
}
$RandomPASS = Get-RandomPassword 20


      


$script:ageList = @{UND = 'UND' ; Czech = 'cs' ; Afrikaans='af';Arabic='ar';Armenian='hy';Aymara='ay';Basque='eu';Belarusian='be';Tibetan='bo';Bosnian='bs';Breton='br';Bulgarian='bg';Catalan='ca';Chinese='zh';Corsican='co';Welsh='cy';Danish='da';German='de';Greek='el';Dutch='nl';English='en';Esperanto='eo';Estonian='et';fil='fil';Finnish='fi';French='fr';Georgian='ka';Irish='ga';Galician='gl';Hebrew='he';Hindi='hi';Croatian='hr';Hungarian='hu';Icelandic='is';Indonesian='id';Italian='it';Javanese='jv';Japanese='ja';Kannada='kn';Korean='ko';Kurdish='ku';Latin='la';Latvian='lv';Lithuanian='lt';Luxembourgish='lb';Macedonian='mk';Malayalam='ml';Maori='mi';Malay='ms';Mongolian='mn';Nepali='ne';Flemish='nl';Norwegian='no';NorwegianBokmal='nb';Polish='pl';Portuguese='pt';Romanian='ro';Russian='ru';Slovak='sk';Slovenian='sl';Spanish='es';Albanian='sq';Sardinian='sc';Serbian='sr';Swedish='sv';Tahitian='ty';Tamil='ta';Tatar='tt';Telugu='te';Thai='th';Turkish='tr';Ukrainian='uk';Urdu='ur';Vietnamese='vi';Volapuk='vo';Walloon='wa';Yiddish='yi'}


#MEDIAINFO

function LOADINFO{
    
    $WPFDefault.IsSelected=$true
    $File = Get-ChildItem $WPftxtSetFolder.Text -Recurse -File | where-object Length -gt 5200kb | Select-Object -First 1
    [int]$AudioCount=Get-MediaInfoValue $File.FullName -Kind General -Parameter 'AudioCount'
    [int]$SubCount=Get-MediaInfoValue $File.FullName -Kind General -Parameter 'TextCount'
    $y=20
    $yImage=22
    $Count=0
    $C=1
    
    $script:IDArray = @();$script:LangArray = @();$script:TNameArray = @();$script:DtrackArray = @();$script:TenableArray = @();$script:ForcedArray = @()
    $Kind = "Audio"
    [int]$IDCount = [int]$AudioCount + [int]$SubCount
    $ImageName= "audio.png"
    $script:Langitem = New-Object System.Windows.Controls.ComboBox
    $script:Slovnik = @('UND','Czech','English','Slovak','Abkhazian','Afh';'Afrikaans','Albanian','Arabic','Armenian','Auxiliary Language Association','Abkhazian','Ale','Aymara','Azerbaijani','Basque','Belarusian','Bho''Tibetan','Bosnian','Breton','Bulgarian','Catalan','Chinese','Corsican','Welsh','Danish','German','Greek';'Dutch','Esperanto','Estonian','fil','Finnish','French','Georgian','Irish','Galician','Hebrew','Hindi','Croatian','Hungarian','Icelandic','Indonesian','Italian','Iro','Javanese','Japanese','Jbo','Kannada','Korean','Kurdish','Latin','Latvian','Lithuanian','Luxembourgish','Macedonian','Malayalam','Maori','Malay','Mongolian','Mus','Nepali','Flemish','Norwegian','NorwegianBokmal','Nso','Polish','Portuguese','Romanian','Russian','Slovenian','Spanish','Sardinian','Serbian','Swedish','Tahitian','Tamil','Tatar','Telugu','Thai','Turkish','Ukrainian','Urdu','Vietnamese','Volapuk','Walloon','Yiddish')
    $c=0
    $t=0
    $script:CheckBoxArray=@()
    $script:CheckBoxArraySub=@()
    $Script:arrayCountText=@()
    $script:LangArray = @()

    for ($i = 1; $i -le $IDCount; $i++){
    
    if($i -gt $AudioCount){
        $Kind = "Text"
        $ImageName= "Subs.png"
        [int]$Count = [int]$i-([int]$AudioCount+1)
    }
    $MediaLang = Get-MediaInfoValue $File.Fullname -Kind $Kind -Index $Count -Parameter 'Language/String'
    $MediaTitle = Get-MediaInfoValue $File.Fullname -Kind $Kind -Index $Count -Parameter 'Title'

    #if($WPFchkcSubOnly.IsChecked -eq $true){
    if($MediaLang -eq "Czech" -or $MediaLang -eq "slovak" -or $MediaLang -eq "English" -and $kind -eq "Text"){
        if($MediaTitle -ne "Commentary"){
            $Pocet=$Count+$AudioCount+1
        $script:arrayCountText+=@($pocet)
        $count++
        }
    }
 
    
    if($MediaLang -eq ""){
        $MediaLang = "UND"
    }
    if($MediaLang -eq "Norwegian Bokmal"){
        $MediaLang="NorwegianBokmal"
    }
    
    $script:ID = New-Object System.Windows.Controls.Label
    $script:Lang = New-Object System.Windows.Controls.ComboBox
    $script:TrackName = New-Object System.Windows.Controls.TextBox
    $script:Dtrack = New-Object System.Windows.Controls.ComboBox
    $script:TEnable = New-Object System.Windows.Controls.ComboBox
    $script:Forced = New-Object System.Windows.Controls.ComboBox
    $CheckBox = New-Object System.Windows.Controls.CheckBox
    $script:AudioImage = new-object System.Windows.Controls.Image
    $script:ID.Name="ID_$i";$script:ID.Content="ID$i"; $ID.Height=25; $ID.Width=35;$ID.VerticalAlignment="Top";$ID.Margin="-460,$y,0,0"
    $Lang.Name = "LangID$($i)"; $script:Lang.Text=$MediaLang; $Lang.Margin="-270,$y,0,0";$Lang.VerticalAlignment="Top";$Lang.Width = 80 ; $Lang.IsReadOnly ; $Lang.FontFamily="Calibri";
    $AudioImage.Margin="-520,$yImage,0,0";$AudioImage.Height=16;$AudioImage.Width=18;$AudioImage.Source=$ImageName;$AudioImage.VerticalAlignment="Top"
    
    foreach($jazyk in $script:Slovnik)
    {
        $Lang.Items.add($jazyk)
    }
    $script:Trackname.Name= "TrackName";$script:TrackName.Text=$MediaTitle; $TrackName.Height = 20 ;$TrackName.Margin="-30,$y,0,0"; $TrackName.Width = 100 ; $TrackName.VerticalAlignment="Top"; $TrackName.TextWrapping="Wrap";$TrackName.FontFamily="Calibri"
    $Dtrack.Name= "Dtrack"; $Dtrack.Margin="150,$y,0,0";$DTrack.VerticalAlignment="Top";$Dtrack.Width = 48 ; $Dtrack.IsReadOnly ; $Dtrack.FontFamily="Calibri";
    $Dtrack.Items.Add("YES")
    $Dtrack.Items.Add("NO")
    $script:TEnable.Name= "TEnable"; $TEnable.Margin="320,$y,0,0";$TEnable.VerticalAlignment="Top";$TEnable.Width = 48 ; $TEnable.IsReadOnly ; $TEnable.FontFamily="Calibri";
    $TEnable.Items.Add("YES")
    $TEnable.Items.Add("NO")
    $script:Forced.Name= "Forced"; $Forced.Margin="500,$y,0,0";$Forced.VerticalAlignment="Top";$Forced.Width = 48 ; $Forced.IsReadOnly ; $Forced.FontFamily="Calibri";
    $CheckBox.Margin="80,$yImage,0,0";$CheckBox.IsChecked=$true
    if($Kind -eq "Audio"){$script:CheckBoxArray+=$CheckBox; $WPFpokus.Addchild($script:CheckBoxArray[$c])}
    elseif($Kind -eq "Text"){$script:CheckBoxArraySUB+=$CheckBox; $WPFpokus.Addchild($script:CheckBoxArraySub[$t]);$t++}
    
    $Forced.Items.Add("YES")
    $Forced.Items.Add("NO")
    $WPFpokus.AddChild($ID)
    $WPFpokus.AddChild($lang)
    $WPFpokus.AddChild($TrackName)
    $WPFpokus.AddChild($DTrack)
    $WPFpokus.AddChild($Tenable)
    $WPFpokus.AddChild($Forced)
    $WPFpokus.Addchild($AudioImage)
    $c++
    $Count++
    if($TrackName.Text -eq "Forced" -or $TrackName.Text -eq "forced" ){
        $TEnable.Items.Add("YES")
        $Forced.Items.Add("NO")          
    }
    
    
    $Lang.SelectedItem = $Lang.Text
    $Dtrack.SelectedIndex = 0
    $Tenable.SelectedIndex = 0
    $Forced.SelectedIndex = 1
    $y= $y+40
    $yImage = $yImage+40   
    $script:IDArray += $ID ; $script:LangArray += $script:Lang; $script:TNameArray += $script:TrackName; $script:DtrackArray +=  $script:Dtrack ; $script:TenableArray += $script:TEnable; $script:ForcedArray += $script:Forced;   
}

    $script:IDArraysub = @();$script:LangArraysub = @();$script:TNameArraysub = @();$script:DtrackArraysub = @();$script:TenableArraysub = @();$script:ForcedArraysub = @()
    $y= $y+40
    $yImage = $yImage+40
    $script:LABELSUB = New-Object System.Windows.Controls.Label
    $script:IDSUB = New-Object System.Windows.Controls.Label
    $script:LangSUB = New-Object System.Windows.Controls.ComboBox
    $script:TrackNameSUB = New-Object System.Windows.Controls.TextBox
    $script:DtrackSUB = New-Object System.Windows.Controls.ComboBox
    $script:TEnableSUB = New-Object System.Windows.Controls.ComboBox
    $script:ForcedSUB = New-Object System.Windows.Controls.ComboBox
    $script:AudioImageSUB = new-object System.Windows.Controls.Image
    $SUBY= $y - 40
    $LABELSUB.Name="Label";$LABELSUB.Content="Nove titulky"; $LABELSUB.Height=25; $LABELSUB.VerticalAlignment="Top";$LABELSUB.Margin="10,$Suby,0,0";$LABELSUB.FontWeight="Bold"
    $IDSUB.Name="ID";$IDSUB.Content="ID"; $IDSUB.Height=25; $IDSUB.VerticalAlignment="Top";$IDSUB.Margin="40,$y,0,0"
    $LangSUB.Name = "LangID"; $LangSUb.Text="UND"; $Langsub.Margin="-270,$y,0,0";$Langsub.VerticalAlignment="Top";$Langsub.Width = 80 ; $Langsub.IsReadOnly ; $Langsub.FontFamily="Calibri";
    $AudioImageSUB.Margin="-520,$yImage,0,0";$AudioImageSub.Height=16;$AudioImageSub.Width=18;$AudioImageSub.Source="Subs.png";$AudioImageSub.VerticalAlignment="Top"
    foreach($jazyk in $Slovnik)
    {

        $Langsub.Items.add($jazyk)
    }
    $Tracknamesub.Name= "TrackName";$TrackNameSub.Height = 20 ;$TrackNameSub.Margin="-20,$y,0,0"; $TrackNameSub.Width = 100 ; $TrackNameSub.VerticalAlignment="Top"; $TrackNameSub.TextWrapping="Wrap";$TrackNameSub.FontFamily="Calibri"
    $Dtracksub.Name= "Dtrack"; $DtrackSub.Margin="150,$y,0,0";$DTracksub.VerticalAlignment="Top";$Dtracksub.Width = 48 ; $Dtracksub.IsReadOnly ; $Dtracksub.FontFamily="Calibri";
    $Dtracksub.Items.Add("YES")
    $Dtracksub.Items.Add("NO")
    $TEnablesub.Name= "TEnable"; $TEnablesub.Margin="320,$y,0,0";$TEnablesub.VerticalAlignment="Top";$TEnablesub.Width = 48 ; $TEnablesub.IsReadOnly ; $TEnablesub.FontFamily="Calibri";
    $TEnablesub.Items.Add("YES")
    $TEnablesub.Items.Add("NO")
    $Forcedsub.Name= "Forced"; $Forcedsub.Margin="500,$y,0,0";$Forcedsub.VerticalAlignment="Top";$Forcedsub.Width = 48 ; $Forcedsub.IsReadOnly ; $Forcedsub.FontFamily="Calibri";
    $Forcedsub.Items.Add("YES")
    $Forcedsub.Items.Add("NO")
    $WPFpokus.AddChild($LABELSUB)
    $WPFpokus.AddChild($IDsub)
    $WPFpokus.AddChild($Langsub)
    $WPFpokus.AddChild($TrackNamesub)
    $WPFpokus.AddChild($DTracksub)
    $WPFpokus.AddChild($Tenablesub)
    $WPFpokus.AddChild($Forcedsub)
    $WPFpokus.Addchild($AudioImagesub)
    $Langsub.SelectedItem = $Lang.Text
    $Dtracksub.SelectedIndex = 0
    $Tenablesub.SelectedIndex = 0
    $Forcedsub.SelectedIndex = 1

    $script:IDArraysub += $script:IDsub ; $script:LangArraysub += $script:LangSub; $script:TNameArraySub += $script:TrackNameSub; $script:DtrackArraySub +=  $script:DtrackSub ; $script:TenableArraySub += $script:TEnableSub; $script:ForcedArraySub += $script:ForcedSub;
  
}

 function RENSUBTITLES {

$Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory 
$WPFconsole.IsSelected = $true

Foreach ($Folder in $Folders){   

$Folders = Get-ChildItem $Folder.FullName -Directory
if($null -eq $Folders){
    $Folders=$Folder
}
foreach($Folder in $Folders){ 

$FolderXML = Get-ChildItem $Folder.FullName -Name FolderSetting.xml 
if($null -eq $folderXML){
}ELSE{
$xmlfile = [XML](Get-Content $FolderXML)

$EXTSUB=$xmlfile.catalog.FolderSetting |Select-Object ExtSub
if(-not $EXTSUB[1].ExtSub -eq ""){

}ELSE{
$WPFrichbox.Document.blocks.Clear()
$WPFRichbox.VerticalScrollBarVisibility = "Visible"
$WPFconsole.IsSelected = $true

$FilesVideo = Get-ChildItem $folder.FullName -Recurse -File | where-object Length -gt 5200kb
$FIlesCount = $filesVideo.Count
$Sub = Get-ChildItem $folder.FullName -Recurse -File -Exclude *.xml*, *.csv*, *.json*, *.txt* | where-object Length -le 5200kb | Select-Object -First 1
$Subs = Get-ChildItem $folder.FullName -Recurse -File -Exclude *.xml*, *.csv*, *.json*, *.txt* | where-object Length -le 5200kb 
$SubsCount = $Subs.Count
$SubtitlesExtension = [System.IO.Path]::GetExtension($Sub)

if($SubsCount -match $FIlesCount){


$mkvmergeoutput = "************************Prejmenovani titulku*************************"
$WPFrichbox.AppendText($mkvmergeoutput) #| Out-String -Width 3))
$WPFrichbox.ScrollToEnd()
[System.Windows.Forms.Application]::DoEvents()

[int]$Count = $Subs.Count - 1 

for ($i = 0; $i -le $Count; $i++ )
{
    
    $VideoOut = $FilesVideo[$i].basename
    $script:SubName = $Folder.Fullname+'\'+$VideoOut + $SubtitlesExtension   
    [string]$Text = $Subs[$i].FullName     

    if($Text -ne  $SubName){
        Rename-Item $Subs[$i].FullName -NeWname $Subname -Force 
    }
    
    $SubOut = $Subs[$i].basename
    $SubOrig = $SubOut + $SubtitlesExtension
         
    $mkvmergeoutput = "$i - $SubOrig byl prejmenovan na $Subname"
    $WPFrichbox.AppendText(($mkvmergeoutput | Out-String -Width 100))
    $WPFrichbox.ScrollToEnd()
    [System.Windows.Forms.Application]::DoEvents()

    }  



 [System.Windows.Forms.Application]::DoEvents()
}Else{
    if($SubsCount -eq 0 ){}else{
    $mkvmergeoutput = "***Titulky nebyli prejmenovany, neodpovida pocet titulku a videa*****"
    $WPFrichbox.AppendText($mkvmergeoutput) #| Out-String -Width 3))
    $WPFrichbox.ScrollToEnd()
    [System.Windows.Forms.Application]::DoEvents()
    $WPFrichbox.AppendText(("**Pokud mate titulky v jinem poctu je potreba prejmenovat ruzny podle nazvu video souboru******" | Out-String ))
    $WPFrichbox.ScrollToEnd()
    [System.Windows.Forms.Application]::DoEvents()
    }

}
}
}
}
}

$mkvmergeoutput = "************************Konec prejmenovani titulku*************************"
$WPFrichbox.AppendText($mkvmergeoutput) #| Out-String -Width 3))
$WPFrichbox.ScrollToEnd()
[System.Windows.Forms.Application]::DoEvents()
}


#Funkce Get-Randomword n?hodn? heslo

function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length
    )
    #$charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789{]+-[*=@:)}$^%;(_!&amp;#?>/|.'.ToCharArray()
    $charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.ToCharArray()
    $rng = New-Object System.Security.Cryptography.RNGCryptoServiceProvider
    $bytes = New-Object byte[]($length)
 
    $rng.GetBytes($bytes)
 
    $result = New-Object char[]($length)
 
    for ($i = 0 ; $i -lt $length ; $i++) {
        $result[$i] = $charSet[$bytes[$i]%$charSet.Length]
    }
 
    return (-join $result)
}

function HASH{
    
    $WPFProgbar.Value = 0
    #$WPFrichbox.Document.blocks.Clear()
    $WPFconsole.IsSelected = $true
    $i=0
    $p=1
    $WPFtxtStatus.Visibility = "Visible"
    $MKVMerge = $WPFtxtInputFilePath.Text
    $Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory
    
    
    foreach($script:Folder in $Folders){
    
    $SUBFOLDER=Get-ChildItem $folder.Fullname -Directory
    if($nul -eq $subfolder){$subfolder = $folder}
    foreach($script:Folder in $SUBFOLDER){
    #if($null -ne $subfolder){$folder = $subfolder}
    $FolderXML = Get-ChildItem $Subfolder.FullName -Name FolderSetting.xml  
    if($null -eq $Folder){
    $FolderXML = Get-ChildItem $folder.FullName -Name FolderSetting.xml
    } 
    if($null -eq $folderXML){
        $mkvmergeoutput = "Nebyl nalezen konfiguracni soubor v adresari "+$Folder.Fullname+"`n"
        $mkvmergeoutput.Font
        $WPFrichbox.AppendText($mkvmergeoutput)
        $WPFrichbox.ScrollToEnd()
        #[System.Windows.MessageBox]::Show('Nebyl nalezen konfigura?n soubor','FolderSetting','Ok','Information')
    }
    elseif($null -ne $folderXML){
     
    
    $mkvmergeoutput = "*******************"+$folder.FullName+"**********************"
    $WPFrichbox.AppendText($mkvmergeoutput)
    $WPFrichbox.ScrollToEnd()
    $file = @($Folder.name.'FolderSetting.xml')   
    $FilesVideo = Get-ChildItem $Folder.FullName -Recurse -File -Exclude *.xml*, *.csv*, *.txt*, *.jpg*, *.png* | where-object Length -gt 5200kb 
    $VideoExtension = [System.IO.Path]::GetExtension($FilesVideo) | Select-Object -First 1
    $SubFile = Get-ChildItem  $Folder.FullName -Recurse -File -Exclude *.xml*, *.csv*, *.txt*, *.jpg*, *.png*| where-object Length -le 5200kb | Select-Object -First 1
    $Subs = Get-ChildItem  $Folder.FullName -Recurse -File -Exclude *.xml*, *.csv*, *.txt*, *.jpg*, *.png*  | where-object Length -le 5200kb 
    $SubtitlesExtension = [System.IO.Path]::GetExtension($SubFile)
    $FilesMedia = Get-ChildItem $Folder.FullName -Recurse -File | where-object Length -gt 5200kb |Select-Object -First 1
    #. .\GetMediaINFO.ps1
    $CountAudio = Get-MediaInfoValue  $FilesMedia.Fullname -Kind General -Parameter 'AudioCount'
    $CountSubs = Get-MediaInfoValue  $FilesMedia.Fullname -Kind General -Parameter 'TextCount'
    $a=1
    [System.Windows.Forms.Application]::DoEvents()
        Foreach ($Files in $FilesVideo.FullName) {
    if($a -gt 1){[System.Windows.Forms.Application]::DoEvents()}
            $SUBFOLDER= Split-Path -Path $Files
            $SubFile = Get-ChildItem  $SUBFOLDER -Recurse -File -Exclude *.xml, *.csv*, *.png*, *.jpg* | where-object Length -le 5200kb | Select-Object -First 1
            $SUBFOLDER= Split-Path -Path $Files
            [string]$XML=$SUBFOLDER
            $FilesXML = Get-ChildItem $XML -File -name FolderSetting.xml
            $IDCount=[int]$CountAudio+[int]$CountSubs
            $file = $SUBFOLDER+"\FolderSetting.xml"
            $xmlfile = [XML](Get-Content $file)
            $Lang=$xmlfile.catalog.FolderSetting |Select-Object Lang
 
        $JoinedString=$Lang[1].Lang -join " "
        $pocet=0  
            
        $SUB=$xmlfile.catalog.FolderSetting.EXTSUBSET
        $SUBPATHXML=$xmlfile.catalog.FolderSetting.EXTSUB
        $Name = [System.IO.Path]::GetFileNameWithoutExtension($Files)
        $subfile=$SubFolder+'\'+$Name + $SUBFile.Extension
        $Subpath= [System.IO.File]::Exists($subfile)
        If($Subpath -eq $true){$subpath="""$subfile""";$subpathremove=$subfile}
        elseif ($Subpath -eq $false){$Subpath=$null;$subpathremove=$null}
        if(-not $SUBPATHXML -eq ""){$Subpath=$SUBPATHXML}
    
        $RandomPASS = Get-RandomPassword 10
        
        $Name = [System.IO.Path]::GetFileNameWithoutExtension($Files)   
        $VideoExtension = $VideoExtension.ToString()
        $MKV = $Files
        #Set Output File Name
        [String]$Output = $Folder.FullName+'\'+$Name + '___MERGED' + '.mkv'
        #[String]$Sub = $WPFtxtSetFolder.Text+'\'+$Name + $SubtitlesExtension
        
        
        $NOGLOBAL = "--no-global-tags --no-track-tags --no-chapters --title "" """
               
      #  $(
        if(-not $WPFchkcTags.IsChecked -eq "false"){
    
            $NOGLOBAL=""
        }
    
       if(-not $WPFchkcSubOnly.IsChecked){
    
        $SubOnly=""
        }
    
       #$Output = $MKV.Directory
       $SUBFOLDER= Split-Path -Path $MKV
    
        $Output = $SubFolder+'\'+$Name + '___MERGED' + '.mkv'
    
        New-Item "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Merge.bat" -Force
        $File = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Merge.bat"
        $removeAudio=$xmlfile.catalog.FolderSetting.AUDIOID
        $subonly=$xmlfile.catalog.FolderSetting.TEXTID
        if($subonly -eq "--subtitle-tracks "){$subonly=$null}
        $Merge = """$MKVMerge"" -o ""$Output"" $removeAudio $SubOnly --language 0:und --track-name 0:$RandomPass --default-track 0:yes --forced-track 0:no $JoinedString $NOGLOBAL ""$MKV"" $SUB $SUBPATH "
        $UTF8 = "chcp 65001"
        $Stream = [System.IO.StreamWriter]::new($File)
        $Stream.WriteLine($UTF8)
        $Stream.WriteLine($merge)
        $Stream.Close()
        & "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Merge.bat"  
        $MKVEXIST= [System.IO.File]::Exists($Output)
        if($MKVEXIST -eq $false){$mkvmergeoutput = "`n***Nedoslo k vytvoreni MKV Merge souboru, zkontrolujte Merge.bat v SCCMKVTOOL***"
        $WPFrichbox.AppendText($mkvmergeoutput)
        $WPFrichbox.ScrollToEnd()
        }
        if(-not $WPFchkcOrigin.IsChecked){
             
            if(-not $null -eq $SUBPATHREMOVE){Remove-Item $SUBPATHREMOVE}
                 
            if(-not $WPFchkcOrigin.IsChecked -eq "True"){
            $MKVEXIST= [System.IO.File]::Exists($mkv)
            if($MKVEXIST -eq "$true"){Remove-Item $MKV}
            elseif($MKVEXIST -eq "$false"){
                $mkvmergeoutput = "`n***Nedoslo k vytvoreni MKV Merge souboru, zkontrolujte Merge.bat v SCCMKVTOOL***"
                $WPFrichbox.AppendText(($mkvmergeoutput | Out-String -Width 3))
                $WPFrichbox.ScrollToEnd()
            }
            
            }
                
            Rename-Item $Output -NewName ($Name + '.mkv')
        }
             
     $JoinedString = ""
     
        $Count=$filesvideo.count
        if($MKVEXIST -eq $true){
            $mkvmergeoutput = "`n$MKV byl zpracovan."
            $WPFrichbox.AppendText($mkvmergeoutput)
            $WPFrichbox.ScrollToEnd()
        }   
        $a++
        $pocet++
        $i++
        [System.Windows.Forms.Application]::DoEvents()
        }       
    
    
    [int]$pct = ($p/$Folders.count)*100
    #update the progress bar
    $WPFProgbar.Value = $pct
    $WPFtxtStatus.Visibility="Visible"
    $WPFtxtStatus.Text = "$PCT %"
    $P++
    if($WPFchkcName.IsChecked -eq $true){
        RENAME-WS
    }
    if($WPFchkcUpload.IsChecked -eq $true ){
        UPLOADWS
    }

    }
    }}
    $WPFtxtStatus.Text = "HOTOVO"
    
    }
    

function rename-ws {
    if($WPFchkcHash.IsChecked -ne $True){
    $Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory  
    $Folders = Get-ChildItem $Folder.FullName -Directory
    }
    elseif($WPFchkcHash.IsChecked -eq $True){
    $Folders=$folder
    }
    
    Foreach ($Folder in $Folders){
   $fileXML= Get-ChildItem $folder.FullName -Directory 
   if($null -eq $filexml){$file = $Folder.Fullname+"\FolderSetting.xml"}
   elseif($null -ne $filexml){$file = $Filexml.Fullname+"\FolderSetting.xml"}
        $xmlfile = [XML](Get-Content $file)
        $NameFile=$xmlfile.catalog.FolderSetting |Select-Object NameFile
        $Series=$xmlfile.catalog.FolderSetting |Select-Object Series
        $Dil=$xmlfile.catalog.FolderSetting |Select-Object Dil
    $FilesVideo = Get-ChildItem $Folder.FullName -Recurse -File | where-object Length -gt 5200kb 


        $HASH = Get-ChildItem $Folder.FullName -Recurse -File | where-object Length -gt 5200kb 
    if($WPFchkcOrigin.IsChecked -and $WPFchkcHash.IsChecked){
        $HASH = Get-ChildItem $Folder.FullName -Recurse -Filter *MERGED*
    }
    
    if($WPFchkcOrigin.IsChecked -and $WPFchkcName.IsChecked -and -not $WPFchkcHash.IsChecked){
    Foreach ($File in $HASH){
    $FileExtension = [System.IO.Path]::GetExtension($File) 
    $newfilename = [System.IO.Path]::GetFileNameWithoutExtension($FILE)+'___MERGED'+$FileExtension 
    
    $SUBFOLDER = Split-Path -Path $File
    [string]$Dest=$subfolder
    $Path="$Dest\$newfilename"

    Copy-Item $file -Destination $Path
    }
    
   }
  
    $random = Get-Random -Maximum 10000000000000000 
    [int]$count=$Dil[1].Dil
    $pocet=1
    $i=1
    
    Foreach ($File in $HASH) {
    $SUBFOLDER = Split-Path -Path $File.Fullname
    [string]$Dest=$subfolder
    $random = Get-Random -Maximum 10000000000000000   
    if(-not $NameFile[1].NameFile -eq "" -and $i -lt 10){
        $Outname=$Dest+'\'+$NameFile[1].NameFile+$Series[1].Series+"0"+$count
    }
    
    if($count -gt 9){
        $Outname=$Dest+'\'+$NameFile[1].NameFile+$Series[1].Series+$count
    }
    
    if($pocet -gt 9 -and $NameFile[1].NameFile -eq ""){
        $Outname=$Dest+'\'+$pocet+$Random
    }

    if($pocet -le 9 -and $NameFile[1].NameFile -eq ""){
    $Outname=$Dest+'\'+0+$pocet+$Random
    }

    $Filepath=$Dest+'\'+$file.PSChildName
      Rename-Item -path $Filepath -NewName $Outname
    $count++ 
    $pocet++
    
    $mkvmergeoutput = "`nSoubor $File byl prejmenovan na $OutName."
    $WPFrichbox.AppendText($mkvmergeoutput)
    $WPFrichbox.ScrollToEnd()
    
    }
    }
    [System.Windows.Forms.Application]::DoEvents()
}


function UPLOADWS{
    $WPFconsole.IsSelected = $true
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "True"){
    $folderdefault= $WPFtxtSetFolder.text+"\links.txt"
    #New-Item $folderdefault -Force
    }
    
    $WPFrichbox.Focus();
    $WPFrichbox.AppendText(("`n************************WS LINKY*************************" | Out-String ))
    

    #Set-Location "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\"
      
    . .\WSToken.ps1

    #$CURLEXE = "curl.exe"
    $CURLEXE = "$($env:WINDIR)\System32\curl.exe"
    $wstoken = $token
    if($null -eq $wstoken){
        [System.Windows.MessageBox]::Show('Nebylo mozne ziskat WSTOKEN, nasledny upload bude anonymni','WSTOKEN','OK','information')
    }   
    #$Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory
    $folders=$folder
    if($WPFchkcHash.IsChecked -ne "false"){
        $Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory 
   }

    foreach($Folder in $Folders){
    
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "True"){
    Add-content $FolderDefault "*************$Folder*************"} 
    $WPFrichbox.AppendText(("*************$Folder*************" | Out-String ))
    $WPFrichbox.ScrollToEnd()
    $WSFolders= Get-ChildItem $Folder.Fullname -Recurse -Directory
    if($null -eq $WSFolders){
        $WSFolders=$Folder
    }

    foreach($Script:Folder in $WSFolders){
    $Files=Get-ChildItem $Folder.FullName -Recurse -File -Exclude *.xml
    $FolderChild=$Folder.FullName
    if( $Folders.count -gt 1){
        if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "True"){
        Add-content $FolderDefault "*************$Folder*************"} 
    $WPFrichbox.AppendText(("*************$Folder*************" | Out-String ))
    
}
    
    function ws_upload_theFile 
    {
        param 
        (
            [Parameter(Mandatory=$True, ValueFromPipeline = $true)]
            [string] $MovieName,
            [string] $folder = "/",
            [Parameter(Mandatory=$True, ValueFromPipeline = $true)]
            [string] $FilePath,
            [string] $private = 0,
            [string] $adult = 0
        )
    
        $ws='https://webshare.cz/api/upload_url?wst='+$wstoken
        $WebshareURL = Invoke-RestMethod -Uri $ws -Method GET 
        $uri = $WebshareURL.response.url
        $CurlArguments = '--request', 'POST', 
        "$uri",
            '--header', "'content-type: multipart/form-data'",
            '--form', "name=$MovieName",
            '--form', "wst=$wstoken",
            '--form', "folder=$folder",
            '--form', "private=$private",
            '--form', "private=$adult",
            '--form', "file=@$FilePath",
            ,'-v'
    
    
        
        $web = & $CURLEXE --progress-bar @CurlArguments
        $webbetter = $web | ConvertFrom-Json
        return $webbetter.ident
    }

 
        if($WPFchkcOrigin.IsChecked){
            $Files = Get-ChildItem -path $Folder.Fullname -Recurse -Include *MERGED*
        }
        if($WPFchkcOrigin.IsChecked -and $WPFchkcName.IsChecked -and -not $WPFchkcHash.IsChecked ){
            $Files = Get-ChildItem -path $Folder.Fullname  -file -Recurse *.xml*, *.csv*, *.json*
        }
        if(-not $WPFchkcOrigin.IsChecked -and -not $WPFchkcName.IsChecked){
            $Files = Get-ChildItem -path $Folder.Fullname  -file -Recurse -Exclude *.xml*, *.csv*, *.json*
        }
        if(-not $WPFchkcOrigin.IsChecked -and $WPFchkcName.IsChecked){
            $Files = Get-ChildItem -path $Folder.Fullname  -file -Recurse -Filter *.
        }
        if($WPFchkcOrigin.IsChecked -and $WPFchkcName.IsChecked -and $WPFchkcUpload.IsChecked){
            $Files = Get-ChildItem -path $Folder.Fullname -file -Recurse -Filter *.
        }
        $file = $Folder.Fullname+"\FolderSetting.xml"
        $xmlfile = [XML](Get-Content $file)
        

        if (Get-Variable 'Serie' -ErrorAction 'Ignore') {
            $False
            Remove-Variable Serie -Force
            Remove-Variable Dil -force
        }

        
        $Dil=$xmlfile.catalog.FolderSetting |Select-Object Dil
        $Serie=$xmlfile.catalog.FolderSetting |Select-Object Series
        $chybdil=$xmlfile.catalog.FolderSetting.ChybDil.Split(',')
        
        $i=0
        [int]$DilXML=$Dil[1].Dil
        
        [int]$SerieXML=$Serie[1].Series
        if($dilxml -eq ""){
            $dilxml=1
        }
        if($SerieXML -eq ""){
            $SerieXML=1
        }
        $dil = $dilXML
        $Serie =  $SerieXML
        $ArraySerie=@();$Arraydil=@();$Arraywsid=@();$ArrayKomplet=@()
        foreach ($item in $Files) {  
            
            if($chybdil -match $dil ){
                for($i=0; $i -lt $chybdil.Length;$i++){
                    if($chybdil -match $dil){$dil=$dil+1}
                }
                    } 
        if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "True"){
        $folderdefault= $WPFtxtSetFolder.text+"\links.txt"}

        $FolderChild=$Folder.FullName
        $Name=$Item.name
        Write-Host "Prave je odesalan soubor: "$item.FullName -ForegroundColor Green
        $wsUUID=ws_upload_theFile -MovieName $item.name -FilePath $item.Fullname 
        
        $ArraySerie+=@($serie)
        $Arraydil+=@($dil)
        $Arraywsid+=@($wsUUID)
        $ArrayKomplet=@($ArraySerie,$Arraydil,$Arraywsid)

        if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "True"){
        Write-Output   "$serie;$dil;$wsUUID" >> $FolderChild\wsid.csv
        Add-content $folderdefault "https://webshare.cz/#/file/$wsUUID/$Name"} 
        $WPFrichbox.AppendText(("https://webshare.cz/#/file/$wsUUID/$Name" | Out-String ))
        $WPFrichbox.ScrollToEnd()
        [System.Windows.Forms.Application]::DoEvents()
        $dil++
        $i++   
    }
    
    if(Get-Variable -name Item -ErrorAction SilentlyContinue){
    . .\scc_post.ps1

    $file = $folder.FullName+"\FolderSetting.xml"
    $xmlfile = [XML](Get-Content $file)

    
    if($xmlfile.catalog.FolderSetting.Movie -eq "True"){   
        
        #if ($WpfchckKoncert.IsChecked -eq $true){$tag= "Concert"}
       $idCSFD=$xmlfile.catalog.FolderSetting.CSFDID;$idTRAKT=$xmlfile.catalog.FolderSetting.TRAKTID;$idTMDB=$xmlfile.catalog.FolderSetting.TMDBID;$idIMDB=$xmlfile.catalog.FolderSetting.IMDBID;$idTVDB=$xmlfile.catalog.FolderSetting.TVDBID
       $film1 = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB"
       $film1.tags = @()
       if ($xmlfile.catalog.FolderSetting.Koncert -eq $true){$tag1="concert"; $film1.tags += $tag1}
       if ($xmlfile.catalog.FolderSetting.Anime -eq $true){$tag2="anime"; $film1.tags += $tag2}
       if ($xmlfile.catalog.FolderSetting.Documentary -eq $true){$tag3="documentary"; $film1.tags += $tag3}
       if ($xmlfile.catalog.FolderSetting.SCCTIP -eq $true){$tag4="scc-tip"; $film1.tags += $tag4}     
       
       #$film2 = scc_add_object_create -tags $tag
       $film1 = scc_add_object_movie $film1 $ArrayKomplet[2] 
       $film1  | ConvertTo-Json -Depth 10 | Out-File "$($Folder.FullName)\scc.json"

       if($WPFchkcHash.IsChecked -eq $true -and $WpfchkcSCC.IsChecked -eq $true){
        scc
   }

    }else{
        
        $idCSFD=$xmlfile.catalog.FolderSetting.CSFDID;$idTRAKT=$xmlfile.catalog.FolderSetting.TRAKTID;$idTMDB=$xmlfile.catalog.FolderSetting.TMDBID;$idIMDB=$xmlfile.catalog.FolderSetting.IMDBID;$idTVDB=$xmlfile.catalog.FolderSetting.TVDBID
        $serial = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB"
        if($xmlfile.catalog.FolderSetting.Dil -eq ""){$dil=1}
        elseif(-not $xmlfile.catalog.FolderSetting.Dil -eq ""){[int]$dil=$xmlfile.catalog.FolderSetting.Dil} 
        if($xmlfile.catalog.FolderSetting.Series -eq ""){$serialID=1}
        elseif(-not $xmlfile.catalog.FolderSetting.Series -eq ""){$SerialID=$xmlfile.catalog.FolderSetting.Series}
        for ($i = 0 ; $i -lt $Files.count ; $i++){
        $serial.tags = @()
        if ($xmlfile.catalog.FolderSetting.Koncert -eq $true){$tag1="concert"; $serial.tags += $tag1}
        if ($xmlfile.catalog.FolderSetting.Anime -eq $true){$tag2="anime"; $serial.tags += $tag2}
        if ($xmlfile.catalog.FolderSetting.Documentary -eq $true){$tag3="documentary"; $serial.tags += $tag3}
        if ($xmlfile.catalog.FolderSetting.SCCTIP -eq $true){$tag4="scc-tip"; $serial.tags += $tag4}   
        $seznam_ws_id_1 = $ArrayKomplet[2][$i] 
        $serial = scc_add_object_tvShow $serial $serialID $Arraydil[$i] $seznam_ws_id_1
            
        $dil++
        }
        $Serial  | ConvertTo-Json -Depth 10 | Out-File "$($Folder.FullName)\scc.json"
        if($WPFchkcHash.IsChecked -eq $true -and $WpfchkcSCC.IsChecked -eq $true){
            scc
       }
    }
} 
    }

}

if($WPFchkcSCC.IsChecked -eq $True -and $WPFchkcHash.IsChecked -eq $false ){
    scc
}
}

$WPFbtnSCCInfo.Add_Click({

if(-not $WPFchckFilm.IsChecked -and -not $WPFchckSerial.IsChecked){
    [System.Windows.MessageBox]::Show('Není zadaná volba Film/Serial','Film/Serial','Ok','Information')
}Else{
if($WPFchckFilm.IsChecked -eq $true -and $WPFchckSerial.IsChecked -eq $true){
    [System.Windows.MessageBox]::Show('Máte vybrané obě volby Film/Serial nutno zapnout jen jednu','Film/Serial','Ok','Information')
}Else{
    
    $WPFServicesmenu.IsSelected = $true
. .\GetDefault.ps1

}
}
})

$WPFbtnUpdateSCC.Add_Click({
    . .\scc_post.ps1
    $SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken | ConvertTo-SecureString
    $tokenDiscord = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword)))) 
    $idCSFD=$WPFtxtCSFD.text;$idTRAKT=$WPFtxtTRAKT.text;$idTMDB=$WPFtxtTMDB.text;$idIMDB=$WPFtxtIMDB.text;$idTVDB=$WPFtxtTVDB.text


    if($WPFchkcPriorCSFD.IsChecked){$priority="csfd"}
    elseif ($WPFchkcPriorTRAKT.IsChecked) {$priority="trakt"}
    elseif ($WPFchkcPriorTMDB.IsChecked) {$priority="tmdb"}
    elseif ($WPFchkcPriorIMDB.IsChecked) {$priority="imdb"}
    elseif ($WPFchkcPriorTVDB.IsChecked) {$priority="tvdb"}
    elseif ($WPFchkcPriorTVDB.IsChecked) {$priority="tvdb"}

    if($WPFchkcDefaultCSFD.IsChecked){$defaultService="csfd"}
    elseif ($WPFchkcDefaultTRAKT.IsChecked) {$defaultService="trakt"}
    elseif ($WPFchkcDefaultTMDB.IsChecked) {$defaultService="tmdb"}
    elseif ($WPFchkcDefaultIMDB.IsChecked) {$defaultService="imdb"}
    elseif ($WPFchkcDefaultTVDB.IsChecked) {$defaultService="tvdb"}

    if(-not $WPFchkcPriorCSFD.IsChecked -and -not $WPFchkcPriorTRAKT.IsChecked -and -not $WPFchkcPriorTMDB.IsChecked -and -not $WPFchkcPriorIMDB.IsChecked -and -not $WPFchkcPriorTVDB.IsChecked){
        $priority="tmdb"
    }

    if($wpfchckStreamOnly.IsChecked){$onlyStream=$false}
    elseif (-not $wpfchckStreamOnly.IsChecked) {$onlyStream=$true}

    if($WPFchckFilm.IsChecked){ 

    $film1 = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB"
    $film1 = scc_add_object_tvShowUpdate $film1 
    $film1 = $film1  | ConvertTo-Json -Depth 10 
    scc_postupdate -json $Film1 -Is_TvShow $False -defaultService $defaultService -Is_streamOnly $OnlyStream -tokenDiscord $tokenDiscord 
    }Else{
    $serial = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB"
    $serial = scc_add_object_tvShowUpdate  $serial #$serialID $dil $seznam_ws_id_1
    $Serial = $Serial  | ConvertTo-Json -Depth 10 | Out-File "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\scc.json"
    $serial = Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\scc.json" | ConvertFrom-Json
    $serial = $serial | ConvertTo-Json -Depth 10
    scc_postupdate -json $serial -Is_TvShow $True -defaultService $defaultService -Is_streamOnly $OnlyStream -tokenDiscord $tokenDiscord
    }
    [System.Windows.MessageBox]::Show('Pokus o update SCC databaze byl odeslan','SCC UPDATE','Ok','Information')
})

function SCC{
    $SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken | ConvertTo-SecureString
    $tokenDiscord = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword))))
    [int]$sec=$xdoc.SCCMKVTOOL.CONFIGURATIONS.TIME
    if($WPFchkcUpload.IsChecked -eq $true -and $sec -ge 1){Start-Sleep -Seconds $sec}
    if($WPFtxtWSIDLinks.text -ne ""){
    if($WPFchkcPriorCSFD.IsChecked){$priority="csfd"}
    elseif ($WPFchkcPriorTRAKT.IsChecked) {$priority="trakt"}
    elseif ($WPFchkcPriorTMDB.IsChecked) {$priority="tmdb"}
    elseif ($WPFchkcPriorIMDB.IsChecked) {$priority="imdb"}
    elseif ($WPFchkcPriorTVDB.IsChecked) {$priority="tvdb"}
    elseif ($WPFchkcPriorTVDB.IsChecked) {$priority="tvdb"}
    
    if($WPFchkcDefaultCSFD.IsChecked){$defaultService="csfd"}
    elseif ($WPFchkcDefaultTRAKT.IsChecked) {$defaultService="trakt"}
    elseif ($WPFchkcDefaultTMDB.IsChecked) {$defaultService="tmdb"}
    elseif ($WPFchkcDefaultIMDB.IsChecked) {$defaultService="imdb"}
    elseif ($WPFchkcDefaultTVDB.IsChecked) {$defaultService="tvdb"}

    if($xmlfile.catalog.FolderSetting.OnlyStream -eq $True){$onlyStream=$false}
    elseif ($xmlfile.catalog.FolderSetting.OnlyStream -ne $True){$onlyStream=$true}
    if($xmlfile.catalog.FolderSetting.Koncert -eq $True){$concert=$true}
    elseif($xmlfile.catalog.FolderSetting.Koncert -ne $True){$concert=$false}

    
    if($xmlfile.catalog.FolderSetting.BlackListCSFD -eq "True"){$BlackListCSFD="&blacklist=csfd"}
    if($xmlfile.catalog.FolderSetting.BlackListTRAKT -eq "True"){$BlackListTRAKT="&blacklist=trakt"}
    if($xmlfile.catalog.FolderSetting.BlackListTMDB -eq "True"){$BlackListTMDB="&blacklist=tmdb"}
    if($xmlfile.catalog.FolderSetting.BlackListIMDB -eq "True"){$BlackListIMDB="&blacklist=imdb"}
    if($xmlfile.catalog.FolderSetting.BlackListTVDB -eq "True"){$BlackListTVDB="&blacklist=tvdb"}
  

    if($xmlfile.catalog.FolderSetting.Koncert -eq "True"){$concert=$true }
    elseif(-not $xmlfile.catalog.FolderSetting.Koncert -eq "True"){$concert=$false }
    if($xmlfile.catalog.FolderSetting.Koncert -eq "False"){$concert=$false }
    
    if($null -eq $priority){
        $priority=$defaultService
    }
   
    $ArrayKomplet=@()
    [int]$serie=$WPFtxtSerie.text
    [int]$dil=$WPFtxtDil.text
        
    foreach($wsid in $wsidlink){
    $ArrayKomplet+=[PSCustomObject]@{Serie=$serie;Dil=$Dil;Wsid=$wsid}
    $dil++
    }
    . .\scc_post.ps1

    if($WpfchckFilm.IsChecked -eq $True){   

        $idCSFD=$wpftxtCSFD.Text;$idTRAKT=$wpftxtTRAKT.Text;$idTMDB=$wpftxtTMDB.Text;$idIMDB=$wpftxtIMDB.Text;$idTVDB=$wpftxtTVDB.Text
        $film1 = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB" -tags $tag
        foreach($komplet in $ArrayKomplet){
        $film1 = scc_add_object_movie $film1 $komplet.wsid 
        }
        
        #$Outfile = $wpftxtSetFolder.Text+"\scc.json"
        #$Serial  | ConvertTo-Json -Depth 10 | Out-File $Outfile -Force
        $film1 = $film1  | ConvertTo-Json -Depth 10 
        scc_post -json $Film1 -Is_TvShow $False -IS_Priority $priority -defaultService $defaultService -Is_streamOnly $OnlyStream -Is_Concert $concert -tokenDiscord $tokenDiscord 
        $WPFrichbox.AppendText(("***Data byla poslana do SCC databaze, zkontrolujte Discord\Stream Cinema***" | Out-String ))   
           
    
    }else{
        $idCSFD=$wpftxtCSFD.Text;$idTRAKT=$wpftxtTRAKT.Text;$idTMDB=$wpftxtTMDB.Text;$idIMDB=$wpftxtIMDB.Text;$idTVDB=$wpftxtTVDB.Text
        $serial = scc_add_object_create -id_csfd "$idCSFD" -id_trakt "$idTRAKT" -id_tmdb "$idTMDB" -id_imdb "$idIMDB" -id_tvdb "$idTVDB"
        foreach($komplet in $ArrayKomplet){
        $serial = scc_add_object_tvShow $serial $Komplet.serie $Komplet.dil $Komplet.Wsid
        }
        #$Outfile = $wpftxtSetFolder.Text+"\scc.json"
        #$Serial  | ConvertTo-Json -Depth 10 | Out-File $Outfile -Force
        $Serial = $Serial  | ConvertTo-Json -Depth 10 
        scc_post -json $serial -Is_TvShow $True -IS_Priority $priority -defaultService $defaultService -Is_streamOnly $OnlyStream -Is_Concert $concert -tokenDiscord $tokenDiscord

        }

    }ELSE{
    $WPFrichbox.AppendText(("************************SCC Databaze*************************" | Out-String ))
    $SCCfolders=$folder
    if($WPFchkcHash.IsChecked -ne "false" -and $WPFchkcUpload.IsChecked -eq $False){
        $SCCFolders = Get-ChildItem $WPFtxtSetFolder.Text -Directory 
   }
   
    foreach($SCCFolder in $SCCFolders){
        $WPFrichbox.AppendText(("*************$SCCFolder*************" | Out-String ))
        $SCCFolders= Get-ChildItem $SCCFolder.Fullname -Recurse -Directory
        if($null -eq $SCCFolders){
            $SCCFolders=$SCCFolder
        }
    
        foreach($SCCFolder in $SCCFolders){
            $FileCount = Get-ChildItem $SCCFolder.FullName -exclude *.xml
            if(-not $null -eq $filecount){
            if( $SCCFolders.count -gt 1){
            $WPFrichbox.AppendText(("*************$SCCFolder*************" | Out-String ))}
            $file = $SCCfolder.FullName+"\FolderSetting.xml"
            $xmlfile = [XML](Get-Content $file)
            $SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken | ConvertTo-SecureString
            $tokenDiscord = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword))))
            if($xmlfile.catalog.FolderSetting.Default -eq "CSFD"){$defaultService="csfd"}
            if($xmlfile.catalog.FolderSetting.Default-eq "TRAKT"){$defaultService="trakt"}
            if($xmlfile.catalog.FolderSetting.Default -eq "TMDB"){$defaultService="tmdb"}
            if($xmlfile.catalog.FolderSetting.Default -eq "IMDB"){$defaultService="imdb"}
            if($xmlfile.catalog.FolderSetting.Default -eq "TVDB"){$defaultService="tvdb"}

        
            if($xmlfile.catalog.FolderSetting.Priority -eq "CSFD"){$priority="csfd"}
            if($xmlfile.catalog.FolderSetting.Priority -eq "TRAKT"){$priority="trakt"}
            if($xmlfile.catalog.FolderSetting.Priority -eq "TMDB"){$priority="tmdb"}
            if($xmlfile.catalog.FolderSetting.Priority -eq "IMDB"){$priority="imdb"}
            if($xmlfile.catalog.FolderSetting.Priority -eq "TVDB"){$priority="tvdb"}

        
            if($xmlfile.catalog.FolderSetting.OnlyStream -eq "True"){$onlyStream=$true}
            elseif ($xmlfile.catalog.FolderSetting.OnlyStream -eq "False") {$onlyStream=$false}
            elseif(-not $xmlfile.catalog.FolderSetting.Koncert -eq "True"){$concert=$false }
        
            
            if($xmlfile.catalog.FolderSetting.BlackListCSFD -eq "True"){$BlackListCSFD="&blacklist=csfd"}
            if($xmlfile.catalog.FolderSetting.BlackListTRAKT -eq "True"){$BlackListTRAKT="&blacklist=trakt"}
            if($xmlfile.catalog.FolderSetting.BlackListTMDB -eq "True"){$BlackListTMDB="&blacklist=tmdb"}
            if($xmlfile.catalog.FolderSetting.BlackListIMDB -eq "True"){$BlackListIMDB="&blacklist=imdb"}
            if($xmlfile.catalog.FolderSetting.BlackListTVDB -eq "True"){$BlackListTVDB="&blacklist=tvdb"}
          
        
            if($xmlfile.catalog.FolderSetting.Koncert -eq "True"){$concert=$true }
            elseif(-not $xmlfile.catalog.FolderSetting.Koncert -eq "True"){$concert=$false }
            if($xmlfile.catalog.FolderSetting.Koncert -eq "False"){$concert=$false }
            
        
            if($null -eq $xmlfile.catalog.FolderSetting.Priority){$priority=$false }

    if($xmlfile.catalog.FolderSetting.Movie -eq "True"){  
          
    $film1 = Get-Content "$($SCCFolder.FullName)\scc.json" | ConvertFrom-Json
    $film1 = $film1  | ConvertTo-Json -Depth 10 
    . .\scc_post.ps1
    scc_post -json $Film1 -Is_TvShow $False -IS_Priority $priority -defaultService $defaultService -Is_streamOnly $OnlyStream -Is_Concert $concert -tokenDiscord $tokenDiscord 
    $WPFrichbox.AppendText(("***Data byla poslana do SCC databaze, zkontrolujte Discord\Stream Cinema***" | Out-String ))   
}Else{

    $serial = Get-Content "$($SCCFolder.FullName)\scc.json" | ConvertFrom-Json
    $serial = $serial | ConvertTo-Json -Depth 10
    . .\scc_post.ps1
    scc_post -json $serial -Is_TvShow $True -IS_Priority $priority -defaultService $defaultService -Is_streamOnly $OnlyStream -Is_Concert $concert -tokenDiscord $tokenDiscord
    $WPFrichbox.AppendText(("***Data byla poslana do SCC databaze, zkontrolujte Discord\Stream Cinema***" | Out-String ))
   }
}
}
}
}
}
    

$WPFbtnKonf.Add_Click({
$xamlfile = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Konfigurace.xaml"
#$xamlfile = "Konfigurace.xaml" 
$inputXAML=Get-Content -Path $xamlfile -Raw
$inputXAML=$inputXAML -replace 'mc:Ignorable="d"','' -replace "x:N","N" -replace '^<Win.*','<Window'
[XML]$XAML=$inputXAML

$reader = New-Object System.Xml.XmlNodeReader $XAML
try {
    $psform2=[WIndows.Markup.XamlReader]::Load($reader)
}catch{
    Write-Host $_.Exception
    throw
}

$xaml.SelectNodes("//*[@Name]") | ForEach-Object {
    try{
        Set-Variable -Name "WPF$($_.Name)" -Value $psform2.Findname($_.Name) -ErrorAction Stop
        }catch{
            throw
        }
    }

    $xdoc = [xml] (Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
    $WPFtxtSetDefault.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH 
    $WPFtxtMergePath.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.MKVMERGE
    $WPFtxtUser.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER
    $WPFtxtpassword.PASSWORD = $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD 
    $WPFtxtDiscordToken.Password =$xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken
    $WPFtxtTime.Text = $xdoc.SCCMKVTOOL.CONFIGURATIONS.TIME
    $WPFchkcForm2Hash.IsChecked = $True
    $WPFchkcForm2Origin.IsChecked = $True
    $WPFchkcForm2Name.IsChecked = $True
    $WPFchkcForm2Tag.IsChecked = $True
    $WPFchkcForm2Upload.IsChecked = $True
    $WPFchkcForm2SubOnly.IsChecked = $True
    $WPFchkcForm2SCC.IsChecked = $True
    $WPFchkcForm2File.IsChecked = $True
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.HASH -eq "False"){
        $WPFchkcForm2Hash.IsChecked = $False}
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.ORIG -eq "False"){
        $WPFchkcForm2Origin.IsChecked = $False}
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.CHANGENAME -eq "False"){
        $WPFchkcForm2Name.IsChecked = $False}   
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.TAGS -eq "False"){
        $WPFchkcForm2Tag.IsChecked = $False} 
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.UPLOADWS -eq "False"){
        $WPFchkcForm2Upload.IsChecked = $False} 
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.SCC -eq "False"){
         $WPFchkcForm2SCC.IsChecked = $False}   
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.SUBONLY -eq "False"){
        $WPFchkcForm2SCC.IsChecked = $False}
    if($xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE -eq "False"){
        $WPFchkcForm2File.IsChecked = $False} 
        
     
    $WPFbtnSetDefault.Add_Click({

        $InputFolder=New-Object System.Windows.Forms.FolderBrowserDialog
        $InputFolder.ShowDialog()
        $WPFtxtSetDefault.Text=$InputFolder.SelectedPath
        $WPFtxtSetFolder.Text=$WPFtxtSetDefault.Text
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH = $WPFtxtSetDefault.Text
        $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
    })

    $WPFbtn_DefaultMkvmerge.Add_Click({

        $InputFilePick=New-Object System.Windows.Forms.OpenFileDialog
        $InputFilePick.ShowDialog()
        $WPFtxtMergePath.Text=$InputFilePick.FileName
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.MKVMERGE = $WPFtxtInputFilePath.Text
        $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
    
    })
    $WPFbtnlogfile.Add_Click({
        Start-Process 'C:\windows\system32\notepad.exe' "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\MKVMerge.log"
        Start-Process 'C:\windows\system32\notepad.exe' "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\PS.log"

    })   

    $WPFbtnSavePass.Add_Click({

        [String]$Username = $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER
        $Username=" "
        if($Username-eq ""){
        $file = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml"
        $element = $xdoc.SCCMKVTOOL.CONFIGURATIONS
        $subelement = $xdoc.CreateElement("USER")
        $element.Appendchild($subelement)
        $xdoc.save($file)
        }

        [String]$Password = $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD 
        $Password=" "
        if($Password -eq "False"){
        $file = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml"
        $element = $xdoc.SCCMKVTOOL.CONFIGURATIONS
        $subelement = $xdoc.CreateElement("PASSWORD")
        $element.Appendchild($subelement)
        $xdoc.save($file)
        }
        
        if($WPFtxtpassword.PASSWORD -eq ""){
            $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD = ""
            $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER = $WPFtxtUser.Text
            $Pass="Empty"
            $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")

        }ELSE{
            $SecretPass = $WPFtxtpassword.PASSWORD | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString 
            $xdoc.SCCMKVTOOL.CONFIGURATIONS.PASSWORD = $SecretPass.ToString()
            $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER = $WPFtxtUser.Text
            $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
            . .\WSToken.ps1
        }

        if( $null -eq $token -or $pass -eq "Empty"){
        [System.Windows.MessageBox]::Show('Nebylo mozne vygenerovat WSTOKEN, zkontrolujte prosim svoje heslo','Password','Ok','Information')
        }       
    })
    $WPFbtnSaveDiscord.Add_Click({


        $SecretDiscordToken = $WPFtxtDiscordToken.Password | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString 
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken = $SecretDiscordToken.ToString()
        
        $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
        
        #$SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken | ConvertTo-SecureString
        #$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword))))
        
        $WPFtxtDiscordToken.Password = $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken

    })

    $WPFbtnkonfFolder.Add_Click({

        invoke-item -path "$($env:USERPROFILE)\AppData\Local\SCCMKVTOOL"
    })

    $WPFbtnSave.Add_Click({

        $UploadWS = $xdoc.SCCMKVTOOL.CONFIGURATIONS.UPLOADWS
        if($UploadWS -eq ""){
        $file = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml"
        $element = $xdoc.SCCMKVTOOL.CONFIGURATIONS
        $subelement = $xdoc.CreateElement("UPLOADWS")
        $element.Appendchild($subelement)
        $xdoc.save($file)
        }

        $xdoc.SCCMKVTOOL.CONFIGURATIONS.MKVMERGE = $WPFtxtMergePath.Text
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH = $WPFtxtSetDefault.Text
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.HASH = $WPFchkcForm2Hash.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.ORIG = $WPFchkcForm2Origin.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.CHANGENAME = $WPFchkcForm2Name.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.TAGS = $WPFchkcForm2Tag.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.UPLOADWS = $WPFchkcForm2Upload.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.USER = $WPFtxtUser.Text
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.SUBONLY = $WPFchkcForm2SubOnly.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.SCC = $WPFchkcForm2SCC.IsChecked.ToString()
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.WSFILE = $WPFchkcForm2File.IsChecked.ToString() 
        $xdoc.SCCMKVTOOL.CONFIGURATIONS.TIME = $WpftxtTime.Text       

        $WPFtxtInputFilePath.Text=$WPFtxtMergePath.Text
        $WPFtxtSetFolder.Text=$WPFtxtSetDefault.Text
        $WPFchkcOrigin.IsChecked = $WPFchkcForm2Origin.IsChecked
        $WPFchkcName.IsChecked = $WPFchkcForm2Name.IsChecked
        $WPFchkcHash.IsChecked = $WPFchkcForm2Hash.IsChecked
        $WPFchkcTags.IsChecked = $WPFchkcForm2Tag.IsChecked
        $WPFchkcUpload.IsChecked = $WPFchkcForm2Upload.IsChecked
        $WPFchkcSubOnly.IsChecked = $WPFchkcForm2SubOnly.IsChecked
        $WPFchkcSCC.IsChecked = $WPFchkcForm2SCC.IsChecked
        $xdoc.Save("$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
            
        [System.Windows.MessageBox]::Show('Konfigurace byla ulozena')
    })
       

    $psform2.ShowDialog()

})

$WPFbtnFolderKonf.Add_Click({

#########################################Created XML Info!!!############################################################


Copy-Item .\FolderSetting.xml -Destination $WPFtxtSetFolder.Text
$File = Get-ChildItem $WPftxtSetFolder.Text -Recurse -File | where-object Length -gt 5200kb | Select-Object -First 1
[int]$AudioCount=Get-MediaInfoValue $File.FullName -Kind General -Parameter 'AudioCount'
[int]$SubCount=Get-MediaInfoValue $File.FullName -Kind General -Parameter 'TextCount'
$file = "$($WPFtxtSetFolder.Text)\FolderSetting.xml"
[int]$IDCount = [int]$AudioCount + [int]$SubCount

$xmlfile = [XML](Get-Content $file)

$newbooknode = $xmlfile.catalog.AppendChild($xmlfile.CreateElement("FolderSetting"))
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("PATH"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtSetFolder.Text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("NameFile"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtName.Text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Series"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtSerie.Text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Dil"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtDil.Text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("ChybDil"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtchybdil.Text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("AudioCount"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($AudioCount)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("SubCount"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($SubCount)) | Out-Null
#$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("ExtSub"))
#$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtSubs.text)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Movie"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckFilm.IsChecked)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("OnlyStream"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckStreamOnly.IsChecked)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Koncert"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckKoncert.IsChecked)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Anime"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckAnime.IsChecked)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Documentary"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckDoc.IsChecked)) | Out-Null
$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("SCCTIP"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFchckscc_tip.IsChecked)) | Out-Null

$xmlfile.save($file) 

if($WPFchkcPriorCSFD.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Priority"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("CSFD")) | Out-Null
}
elseif($WPFchkcPriorTRAKT.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Priority"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TRAKT")) | Out-Null
}
elseif($WPFchkcPriorTMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Priority"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TMDB")) | Out-Null
}
elseif($WPFchkcPriorIMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Priority"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("IMDB")) | Out-Null
}
elseif($WPFchkcPriorTVDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Priority"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TVDB")) | Out-Null
}


if($WPFchkcDefaultCSFD.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Default"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("CSFD")) | Out-Null
}
elseif($WPFchkcDefaultTRAKT.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Default"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TRAKT")) | Out-Null
}
elseif($WPFchkcDefaultTMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Default"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TMDB")) | Out-Null
}
elseif($WPFchkcDefaultIMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Default"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("IMDB")) | Out-Null
}
elseif($WPFchkcDefaultTVDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Default"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("TVDB")) | Out-Null
}

if($WPFchkcBlacklistCSFD.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("BlackListCSFD"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("True")) | Out-Null
}
elseif($WPFchkcBlacklistTRAKT.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("BlackListTRAKT"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("True")) | Out-Null
}
elseif($WPFchkcBlacklistTMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("BlackListTMDB"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("True")) | Out-Null
}
elseif($WPFchkcBlacklistIMDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("BlackListIMDB"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("True")) | Out-Null
}
elseif($WPFchkcBlacklistTVDB.IsChecked -eq "True"){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("BlackListTVDB"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("True")) | Out-Null
}

if(-not $WPFtxtCSFD.text -eq ""){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("CSFDID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtCSFD.text)) | Out-Null
}
if(-not $WPFtxtTRAKT.text -eq ""){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("TRAKTID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtTRAKT.text)) | Out-Null
}
if(-not $WPFtxtTMDB.text -eq ""){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("TMDBID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtTMDB.text)) | Out-Null
}
if(-not $WPFtxtIMDB.text -eq ""){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("IMDBID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtIMDB.text)) | Out-Null
}
if(-not $WPFtxtTVDB.text -eq ""){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("TVDBID"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode($WPFtxtTVDB.text)) | Out-Null
}




$IDSETAUDIO=@()
$IDSETTEXT=@()
#$count=$AudioCount+$SubCount
$l=1
for ($i = 0; $i -lt $AudioCount; $i++){
if($i -le $Idcount -and $CheckBoxArray[$i].IsChecked -eq $true ){
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Lang"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("--language "+$l+":"+$ageList[$LangArray[$i].Text]+" --track-name "+$l+":"+$TNameArray[$i].Text.Replace(" ", "")+" --default-track "+$l+":"+$Dtrackarray[$i].Text+" --track-enabled-flag "+$l+":"+$TenableArray[$i].Text+" --forced-display-flag "+$l+":"+$ForcedArray[$i].Text)) | Out-Null

    $idsetaudio+=$i+1
}
$l++
}

$l=0
for ($a = 0; $a -lt $SubCount; $a++){
$b=$a+1
    if($CheckBoxArraySub[$a].IsChecked -eq $True){
    $idsettext+=$AudioCount+$a+1
    $id=$AudioCount+$a+1 
    $newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("Lang"))
    $newauthorelement.AppendChild($xmlfile.CreateTextNode("--language "+$id+":"+$ageList[$LangArray[$b].Text]+" --track-name "+$id+":"+$TNameArray[$b].Text.Replace(" ", "")+" --default-track "+$id+":"+$Dtrackarray[$b].Text+" --track-enabled-flag "+$id+":"+$TenableArray[$b].Text+" --forced-display-flag "+$id+":"+$ForcedArray[$b].Text)) | Out-Null
     
    }
    $l++   
}




$AUDIOSTAY=$idsetaudio -join ","
$TEXTSTAY=$idsettext -join ","
$AUDIOID="--audio-tracks $AUDIOSTAY"
$TEXTID="--subtitle-tracks $TEXTSTAY"
if($TEXTSTAY -eq ""){$TEXTID="--no-subtitles"}
if($WPFchkcSubOnly.IsChecked -eq $True){

$TEXTSTAY=$arrayCountText -join ","
    if($TEXTSTAY -eq ""){$TEXTID="--no-subtitles"}
    elseif($TEXTSTAY -ne ""){$TEXTID="--subtitle-tracks $TEXTSTAY"}
}  


$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("EXTSUBSET"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode("--language "+0+":"+$ageList[$LangArraySub[0].Text]+" --track-name "+0+":"+$TNameArraySub[0].Text.Replace(" ", "")+" --default-track "+0+":"+$DtrackarraySub[0].Text+" --track-enabled-flag "+0+":"+$TenableArraySub[0].Text+" --forced-display-flag "+0+":"+$ForcedArraySub[0].Text)) | Out-Null

$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("AUDIOID"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($AudioID)) | Out-Null

$newauthorelement = $newbooknode.AppendChild($xmlfile.CreateElement("TEXTID"))
$newauthorelement.AppendChild($xmlfile.CreateTextNode($TEXTID)) | Out-Null

if($WPFchckStreamOnly.IsChecked -eq $false){
$service=@($WPFchkcDefaultCSFD,$WPFchkcDefaultTRAKT,$WPFchkcDefaultTMDB,$WPFchkcDefaultIMDB,$WPFchkcDefaultTVDB)
$check=$null
for ($i=0;$i -le $service.Length;$i++ ){
if($service[$i].IsChecked -eq  $true){$check=1}
}

if($null -eq $check -and $WpfchkcSCC.IsChecked -eq $true ){    
[System.Windows.MessageBox]::Show('Konfigurace nebyla ulozena, nemate nastavenou defaultni sluzbu','FolderSetting','Ok','Information')
Remove-Item "$($WPFtxtSetFolder.Text)\FolderSetting.xml"  
}else{

$xmlfile.save($file) 

[System.Windows.MessageBox]::Show('Konfigurace byla ulozena','FolderSetting','Ok','Information')
}

}
$xmlfile.save($file) 
MakeNewForm
})

$WPFbtn_mkvmerge.Add_Click({
    $InputFilePick=New-Object System.Windows.Forms.OpenFileDialog
    $InputFilePick.ShowDialog()
    $WPFtxtInputFilePath.Text=$InputFilePick.FileName   

})

$WPFbtn_WSID.Add_Click({
    $InputFilePick=New-Object System.Windows.Forms.OpenFileDialog
    $InputFilePick.ShowDialog()
    $WPFtxtWSIDLinks.Text=$InputFilePick.FileName 
    $txt = Get-Content $WPFtxtWSIDLinks.Text
    $radky = $txt.split(0x0A)
    $script:wsidlink=@()
    $i=4
    foreach($radek in $radky){    
    if($radek -like "*beta*"){
        $split=$radek.split('/')
        $script:wsidlink+=$split[$i]
        $joinWSID=$wsidlink -join ","
    }Else{
        $split=$radek.split('/')
        $script:wsidlink+=$split[$i]
        $joinWSID=$wsidlink -join ","}
    }
    

    Write-Host $joinWSID
})


$WPFbtnSetFolder.Add_Click({

    
    $FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{
        SelectedPath = $xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH
    }
    
    [void]$FolderBrowser.ShowDialog()
    $FolderBrowser.SelectedPath
    $WPftxtSetFolder.Text=$FolderBrowser.SelectedPath

    #$InputFolder=New-Object System.Windows.Forms.FolderBrowserDialog
    #$inputFolder.SelectedPath=$xdoc.SCCMKVTOOL.CONFIGURATIONS.DEFAULTPATH
    #$WPFtxtSetFolder.Text=$InputFolder.SelectedPath
    #$InputFolder.ShowDialog()


})

$WPFbtnrefresh.Add_Click({
    
    MakeNewForm
    

})

$WPFbtnDEL.Add_Click({
    $xdoc = [xml] (Get-Content "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\Settings.xml")
    $SecretPassword= $xdoc.SCCMKVTOOL.CONFIGURATIONS.DiscordToken | ConvertTo-SecureString
    $tokenDiscord = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($SecretPassword)))) 

    $WSID=$wpftxtDel.Text
    $URI= "https://plugin.sc2.zone/api/db/admin/stream/"+$WSID+"?access_token=$tokenDiscord"
    
    if($wpfchckFilm.IsChecked -eq $true){$mediatype="movie"}
    elseif($wpfchckSerial.IsChecked -eq $true){$mediatype="tvshow"}
    if($wpftxtCSFD.Text -ne ""){$service="csfd";$id=$WPFtxtCSFD.Text}
    elseif($wpftxtTRAKT.Text -ne ""){$service="trakt";$id=$WPFtxtTRAKT.Text}
    elseif($wpftxtTMDB.Text -ne ""){$service="tmdb";$id=$WPFtxtTMDB.Text}
    elseif($wpftxtIMDB.Text -ne ""){$service="imdb";$id=$WPFtxtIMDB.Text}
    elseif($wpftxtTVDB.Text -ne ""){$service="tvdb";$id=$WPFtxtTVDB.Text}
    $URITYPE = "https://plugin.sc2.zone/api/db/admin/media/"+$mediatype+"/"+$service+"/"+$id+"?access_token="+$tokenDiscord
    if($wpfchckFilm.IsChecked -eq $false -and $wpfchckSerial.IsChecked -eq $False){
    Try{
    Invoke-RestMethod -Uri $URI -Method Delete
    }Catch{
        if($_.ErrorDetails.Message){
            $APIERROR=$_.ErrorDetails.Message
            [System.Windows.Forms.MessageBox]::Show("$APIERROR",'WSID','Ok','error')
        
        }
    }
    }ELSE{
    Try{
        Invoke-RestMethod -Uri $URITYPE -Method Delete
    }Catch{
        if($_.ErrorDetails.Message){
            $APIERROR=$_.ErrorDetails.Message
            [System.Windows.Forms.MessageBox]::Show("$APIERROR",'WSID','Ok','error')
        } 
    }   
} if($null -eq $APIERROR){[System.Windows.Forms.MessageBox]::Show("Pozadavek byl zpracovan",'DELETE','Ok','information') }
})


$WPFbtnWSID.Add_Click({
    $WSID=$wpftxtWSID.Text
    $SCCFilter = Invoke-RestMethod -Uri "https://plugin.sc2.zone/api/stream/ident/$WSID"
    if($SCCFilter -eq ''){
        [System.Windows.Forms.MessageBox]::Show("Vase WSID:$WSID nebylo nalezeno v databazi SCC",'WSID','Ok','error') 
    }elseif(-not $null -eq $SCCFilter){[System.Windows.Forms.MessageBox]::Show("Vase WSID: $WSID bylo nalezeno v databazi SCC",'WSID','Ok','Information') }


})  



$WPFbtnRUN.Add_Click({

    $Folders = Get-ChildItem $WPFtxtSetFolder.Text -Directory
    Write-host $folders.count

#if($folders.count -eq "0" ){

   # [System.Windows.MessageBox]::Show('V zadanm adres?i nebyli nalezeny dn sloky ke zpracovn','Ws','Ok','Information')
#}ELSE{

    $Video = Get-ChildItem $WPFtxtSetFolder.Text -Recurse -File | where-object Length -gt 5200kb
    $Subs = Get-ChildItem $WPFtxtSetFolder.Text -Recurse -File | where-object Length -le 5200kb
    $Files = Get-ChildItem $WPFtxtSetFolder.Text
    $WPFtxtNumberFile.text = $Video.Count
    $WPFtxtNumberSubs.text = $Subs.Count
    if($WPFtxtInputFilePath.Text -eq ""){
        [System.Windows.Forms.MessageBox]::Show("Není zadaná cesta k nástroji MKVTOOLNix") 
    }ELSE{
    if($WPFtxtSetFolder.Text -eq ""){
            [System.Windows.Forms.MessageBox]::Show("Neni zadany vstupni adresář")   
    }ELSE{
    if($Files.Count -eq 0){
        [System.Windows.Forms.MessageBox]::Show("V zadanem adresari nejsou zadne soubory")
    }ELSE{
    if($WPFchkcUpload.IsChecked -and -not $WPFchkcName.IsChecked -and -not $WPFchkcHash.IsChecked -and -not $WPFchkcSCC.IsChecked){
        
      UPLOADWS
      [System.Windows.MessageBox]::Show('Vase soubory byly nahrany na Webshare','Ws','Ok','Information')
        
    }ELSE{
    if($WPFchkcHash.IsChecked -eq "True"){
        
    RENSUBTITLES
    HASH
            
    }ELSE{
  
    if($WPFchkcName.IsChecked -eq "True" -and -not $WPFchkcUpload.IsChecked ){
        rename-ws
        [System.Windows.Forms.MessageBox]::Show("Vase soubory byly prejmenovany")
    }ELSE{
    

    if($WPFchkcName.IsChecked -eq "True" -and $WPFchkcUpload.IsChecked ){
    #RENAME-WS
    UPLOADWS
    [System.Windows.MessageBox]::Show('Vase soubory byly nahrany na Webshare','Ws','Ok','Information')
    }
    if($WPFchkcSCC.IsChecked -eq "True" -and -not $WPFchkcUpload.IsChecked -eq "False" -and -not $WPfchckHash.IsChecked -eq "False" ){
        scc
        [System.Windows.Forms.MessageBox]::Show("Vase soubory byly nahrany do databaze SCC")
        
    }ELSE{
        if($WPFchkcSCC.IsChecked -eq "True" -and $WPFchkcUpload.IsChecked -eq "True" -and -not $WPfchckHash.IsChecked -eq "False" ){
            UPLOADWS
            
            [System.Windows.Forms.MessageBox]::Show("Vase soubory byly nahrany do databaze SCC")
            
        }  
    }


    }

  
}     
}
}
}
}
if($WPFchkcEND.IsChecked -eq $true){shutdown /s} 
})



$WPFbtnMediaInfo.Add_Click({

    LOADINFO
    
        
})



#===========================================================================
# Shows the form
#===========================================================================
$Form.ShowDialog() | Out-Null
}

MakeForm

