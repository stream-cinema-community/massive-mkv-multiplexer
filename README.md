                 ******************************
                 *  Massive MKV Multiplexer   *
                 *  stručný popis prostředí   *
                 ******************************
Pro první spuštění použijte možnost spustit jako správce, do pc se doinstaluje knihovna pro získávání informací z MKV. Dle nastavení Windows bude potřeba při instalaci potvrdit že souhlasíte s instalací.


***************** 
* MKV adresář:  *
*****************
Cesta k MKVToolnix, default nastavení: "C:\Program Files\MKVToolNix\mkvmerge.exe"

*********************
* Vstupní adresář:  *
*********************
Nastavíme cestu k videosouborům, které chceme spracovat (cez browse, alebo skopírovaním cesty),
poté načteme video, audio a titulkové dáta z prvního videosouboru => MediaInfo

*****************
*   MediaInfo:  *
*****************
Načte dáta z prvního videosouboru a zobrazí ich v tabulce: audio a titulky.

******************************
*  Zachovat původní soubory: *
******************************
nastaví, aby se nemazali originálni soubory.
Akce se automaticky přidá do tlačidel:
                             MULTIPLEXING 1
                             MULTIPLEXING 2
                             PŘEJMENOVÁNÍ

***************************************
*  TABULKY NASTAVENÍ: Audio, titulky: * 
***************************************
Možnost změnit nasledovní vlastnosti stop: Jazyk; Track name; Default track; Track enabled; Forced display

 DEFAULTNÍ NASTAVENÍ: 
   a) VIDEOSTOPA: nenastaví se
         - Trackname = automaticky se nastaví náhodní řeťezec znaků
         - Jazyk = nenastaví se
         - Default track = Yes
         - Track enabled = Yes
         - Forced display = No

   b) AUDIOSTOPA: v tabulce se dají dělat specifické změny, ktoré se projeví po multiplexování
          - Trackname = nie je nastavené (takže žiaden text)
          - Jazyk = skopíruje se z MediaInfo, dá se nastavit (viz. kódy jazyků)
          - Default track = Yes
          - Track enabled = Yes
          - Forced display = No

   c) TITULKOSTOPA: v tabulce se dají dělat specifické změny, ktoré se projeví po multiplexování
          - Trackname = nie je nastavené (takže žiaden text)
          - Jazyk = skopíruje se z MediaInfo, dá se nastavit (viz. kódy jazyků)
          - Default track = No
          - Track enabled = Yes
          - Forced display = No

***************************************
*  TABULKY NASTAVENÍ: Nové titulky:   * 
***************************************


********************************
* MULTIPLEXING - TLAČIDLÁ AKCE *
********************************
 MULTIPLEXING 1: multiplexing bez změny názvy souborů + změna HASH v Trackname videostopy
  a) názvy nových souborům budou stejné jako originální soubory (originály budou smazány),
  b) "Zachovat původní soubory" - k novým souborům přidá "MERGE" a originály se nezmažou
  c) smaže global tagy
  d) vygeneruje náhodný Trackname při videostopě
  e) smaže názvy audiostop a titulkostop,
  f) když najde v Trackname titulkostopy originálu slovo "Forced"/"forced",
     automaticky nastaví při titulkostopě:
                             Track name = Forced
                             Default track = No
                             Track enabled = No
                             Forced display = Yes
  g) promítne nastavení v tabulce Audio a titulky do nastavení nového súboru.


 MULTIPLEXING 2: multiplexing + změna názvu souboru + změna HASH v Trackname videostopy
  a) generuje náhodné nové názvy souborů,
  b) "Zachovat původní soubory" - originály se nezmažou
  c) vymaže global tagy
  d) vygeneruje náhodný Track name pri videostopě
  e) smaže názvy audiostop a titulkostop,
  f) když najde v Trackname titulkostopy originálu slovo "Forced"/"forced",
     automaticky nastaví při titulkostopě:
                             Track name = Forced
                             Default track = No
                             Track enabled = No
                             Forced display = Yes
  g) promítne nastavení v tabulce Audio a titulky do nastavení nového súboru.


 PŘEJMENOVÁNÍ: změna názvu souboru + změna HASH v Trackname videostopy
  a) "Zachovat původní soubory" - na tento proces nemá vlyv
  a) generuje náhodné nové názvy souborů
  b) symaže global tagy
  c) generuje náhodný Track name pri videostope



