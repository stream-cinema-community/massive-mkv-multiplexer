
function scc_post 
{
    param 
    (
        [Parameter(Mandatory=$true)]$json, # JsonObjekt [json objekt]
        [Parameter(Mandatory=$true)]$Is_TvShow, # Je to serial? [bool]  true << >> false
        [Parameter(Mandatory=$true)]$defaultService, # Default služba [text] tmdb, trakt, imdb, csfd ,tvdb
        [Parameter(Mandatory=$true)]$Is_streamOnly, # zpracovat pouze dotaz a neaktualizvat cely root serialu či filmu [book] true << >> false
        [Parameter(Mandatory=$true)]$tokenDiscord, # discord token "string"
        [Parameter(Mandatory=$true)]$Is_Concert, # Je to Koncert? [bool]  true << >> false
        [Parameter(Mandatory=$true)]$IS_Priority # Default služba [text] tmdb, trakt, imdb, csfd ,tvdb
     
    )
    $type = ""
    $service = ""
    $st_only = ""
    
    if ($Is_TvShow){$type = "&type=tvshow"} else{$type = "&type=movie"}
    if ($Is_Concert -eq $true) {$Concert = "&is_concert=1"}
    elseif ($Is_Concert -eq $false) {$Concert = "&is_concert=0"}
    if ($defaultService -eq "tmdb") {$service = "&default_service=tmdb"}
    elseif ($defaultService -eq "trakt") {$service = "&default_service=trakt"}
    elseif ($defaultService -eq "imdb") {$service = "&default_service=imdb"}
    elseif ($defaultService -eq "csfd") {$service = "&default_service=csfd"}
    elseif ($defaultService -eq "tvdb") {$service = "&default_service=tvdb"}
    elseif ($defaultService -eq "1") {$service = ""}
    elseif ($defaultService) {$service = ""}
    if ($Is_streamOnly -eq $true) {$st_only = "&streams_only=0"} 
    elseif ($Is_streamOnly -eq $false) {$st_only = "&streams_only=1"}
    if ($IS_Priority -eq "tmdb") {$Priorityservice = "&priority=tmdb"}
    elseif ($IS_Priority -eq $false) {$Priorityservice = ""}
    elseif ($IS_Priority -eq "trakt") {$Priorityservice = "&priority=trakt"}
    elseif ($IS_Priority -eq "imdb") {$Priorityservice = "&priority=imdb"}
    elseif ($IS_Priority -eq "csfd") {$Priorityservice = "&priority=csfd"}
    elseif ($IS_Priority -eq "tvdb") {$Priorityservice= "&priority=tvdb"}




    $Body = $json
    $header = @{
        "Content-Type"="application/json;charset=utf-8"
        "X-Uuid"="PowershellScript"
        "User-Agent"="PowershellScript"
    }
    New-Item "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\SCCAPI.txt" -Force
    $File = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\SCCAPI.txt"
    $tajemmnykod = "?access_token=" + $tokenDiscord
    $SCCAPI= "https://plugin.sc2.zone/api/db/add$tajemmnykod$type$service$Priorityservice$st_only$concert$BlackListCSFD$BlackListTRAKT$BlackListTMDB$BlackListIMDB$BlackListTVDB" 
    $Stream = [System.IO.StreamWriter]::new($File)
    $Stream.WriteLine($SCCAPI)
    $Stream.Close()
    #http://plugin.sc2.zone/api/db/add?access_token=XCDGEaMzJh90tDG8GxnUFmuf7ZZ2tc&amp;type=movie&amp;default_service=csfd&amp;streams_only=0&amp;priority=csfd&amp;blacklist=tmdb&amp;blacklist=tvdb
    Try{
        Invoke-RestMethod -Uri "https://plugin.sc2.zone/api/db/add$tajemmnykod$type$service$Priorityservice$st_only$concert$BlackListCSFD$BlackListTRAKT$BlackListTMDB$BlackListIMDB$BlackListTVDB"  -Headers $header -Body $Body -ContentType "application/json" -Method Post -UserAgent "PowershellScript"
        #
        }Catch{
            if($_.ErrorDetails.Message){
                $APIERROR=$_.ErrorDetails.Message
                [System.Windows.Forms.MessageBox]::Show("$APIERROR",'Přístup na SCC','Ok','error')
            
            }
        }

    $WPFrichbox.AppendText(("********$Post*******" | Out-String  ))  
    
}

function scc_postupdate 
{
    param 
    (
        [Parameter(Mandatory=$true)]$json, # JsonObjekt [json objekt]
        [Parameter(Mandatory=$true)]$Is_TvShow, # Je to serial? [bool]  true << >> false
        [Parameter(Mandatory=$true)]$defaultService, # Default služba [text] tmdb, trakt, imdb, csfd ,tvdb
        [Parameter(Mandatory=$true)]$Is_streamOnly, # zpracovat pouze dotaz a neaktualizvat cely root serialu či filmu [book] true << >> false
        [Parameter(Mandatory=$true)]$tokenDiscord # discord token "string"

     
    )
    $type = ""
    $service = ""
    $st_only = ""
    
    if ($Is_TvShow){$type = "&type=tvshow"} else{$type = "&type=movie"}
    if ($Is_Concert -eq $true) {$Concert = "&is_concert=1"}
    elseif ($Is_Concert -eq $false) {$Concert = "&is_concert=0"}
    if ($defaultService -eq "tmdb") {$service = "&default_service=tmdb"}
    elseif ($defaultService -eq "trakt") {$service = "&default_service=trakt"}
    elseif ($defaultService -eq "imdb") {$service = "&default_service=imdb"}
    elseif ($defaultService -eq "csfd") {$service = "&default_service=csfd"}
    elseif ($defaultService -eq "tvdb") {$service = "&default_service=tvdb"}
    elseif ($defaultService -eq "") {$service = ""}
    elseif (!$defaultService) {$service = ""}
    if ($Is_streamOnly -eq $true) {$st_only = "&streams_only=1"} 
    elseif ($Is_streamOnly -eq $false) {$st_only = "&streams_only=0"}
    elseif (!$Is_streamOnly) {$st_only = "&streams_only=1"}
    if ($IS_Priority -eq "tmdb") {$Priorityservice = "&priority=tmdb"}
    elseif ($IS_Priority -eq $false) {$Priorityservice = ""}
    elseif ($IS_Priority -eq "trakt") {$Priorityservice = "&priority=trakt"}
    elseif ($IS_Priority -eq "imdb") {$Priorityservice = "&priority=imdb"}
    elseif ($IS_Priority -eq "csfd") {$Priorityservice = "&priority=csfd"}
    elseif ($IS_Priority -eq "tvdb") {$Priorityservice= "&priority=tvdb"}




    $Body = $json
    $header = @{
        "Content-Type"="application/json;charset=utf-8"
        "X-Uuid"="PowershellScript"
        "User-Agent"="PowershellScript"
    }
    New-Item "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\SCCAPI.txt" -Force
    $File = "$($env:USERPROFILE)\AppData\Local\SCCMKVTool\SCCAPI.txt"
    $tajemmnykod = "?access_token=" + $tokenDiscord
    $SCCAPI= "https://plugin.sc2.zone/api/db/add$tajemmnykod$type$service$Priorityservice$st_only$concert$BlackListCSFD$BlackListTRAKT$BlackListTMDB$BlackListIMDB$BlackListTVDB" 
    $Stream = [System.IO.StreamWriter]::new($File)
    $Stream.WriteLine($SCCAPI)
    $Stream.Close()
    #http://plugin.sc2.zone/api/db/add?access_token=XCDGEaMzJh90tDG8GxnUFmuf7ZZ2tc&amp;type=movie&amp;default_service=csfd&amp;streams_only=0&amp;priority=csfd&amp;blacklist=tmdb&amp;blacklist=tvdb
    $Post = Invoke-RestMethod -Uri "https://plugin.sc2.zone/api/db/add$tajemmnykod$type$service$Priorityservice$st_only$concert$BlackListCSFD$BlackListTRAKT$BlackListTMDB$BlackListIMDB$BlackListTVDB" -Headers $header -Body $Body -ContentType "application/json" -Method Post -UserAgent "PowershellScript"
    $WPFrichbox.AppendText(("********$Post*******" | Out-String  ))  
    
    
}


function scc_add_object_create 
{
    param 
    (
        [Parameter(Mandatory=$false)][string]$id_tmdb,
        [Parameter(Mandatory=$false)][string]$id_trakt,
        [Parameter(Mandatory=$false)][string]$id_imdb,
        [Parameter(Mandatory=$false)][string]$id_csfd,
        [Parameter(Mandatory=$false)][string]$id_tvdb,
        [Parameter(Mandatory=$false)][string]$tags
    )

    $scc_object = @{}
    $scc_object.services = @{}
    $scc_object.items = @()

    if ($id_tmdb)  {$scc_object.services.Add("tmdb",[string]$id_tmdb)}
    if ($id_trakt) {$scc_object.services.Add("trakt",[string]$id_trakt)}
    if ($id_imdb)  {$scc_object.services.Add("imdb",[string]$id_imdb)}
    if ($id_csfd)  {$scc_object.services.Add("csfd",[string]$id_csfd)}
    if ($id_tvdb)  {$scc_object.services.Add("tvdb",[string]$id_tvdb)}
    
    
    return $scc_object
   
}


function scc_add_object_tvShow 
{
    param 
    (
        [Parameter(Mandatory=$true)]$scc_object,
        [Parameter(Mandatory=$true)][int]$season,
        [Parameter(Mandatory=$true)][int]$episode,
        [Parameter(Mandatory=$true)]$streams
    )
    $tv_show_object = @{}
    $tv_show_object.season = $season
    $tv_show_object.episode = $episode
    $tv_show_object.streams = @($streams)

    $scc_object.items += , $tv_show_object
    return $scc_object
    
}

function scc_add_object_tvShowdefault
{
    param 
    (
        [Parameter(Mandatory=$true)]$scc_object

    )
    $tv_show_object = @{}

    return $scc_object
    
}

function scc_add_object_tvShowUpdate 
{
    param 
    (
        [Parameter(Mandatory=$true)]$scc_object

    )
    $tv_show_object = @{}

    return $scc_object
    
}

function scc_add_object_movie
{
    param 
    (
        [Parameter(Mandatory=$true)]$scc_object,
        [Parameter(Mandatory=$true)]$streams
   )

    $movie_object = @{}
    $movie_object.streams = @($streams)
    $scc_object.items += , $movie_object
    return $scc_object
    
}





