﻿#########################################
#### Webshare.cz Togen Getter ###########
#### Created By Noway 2022 ###########
########################################

function GetHttpContent($body,$uri) {
    $contentType = 'application/x-www-form-urlencoded' 
    [xml]$cont = Invoke-WebRequest -Method POST -Uri $uri -body $body -ContentType $contentType
    return $cont
}

$wsid = "S89pTJE7Mq" 

$bodySalt = @{ident="$wsid";
          charset='UTF-8'}
$links = GetHttpContent $bodySalt "https://webshare.cz/api/file_link/"
$link = $links.response.link
$link
